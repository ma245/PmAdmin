(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
    
    // Toggle the side navigation when window is resized below 480px
    if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
      $("body").addClass("sidebar-toggled");
      $(".sidebar").addClass("toggled");
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });
  
	 //popovers
	$('[data-toggle="popover"]').popover();
	//select2
	$('.chosen-select').each(function () {
		$(this).select2({
			theme: 'bootstrap4',
			width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
			placeholder: $(this).data('placeholder'),
			allowClear: Boolean($(this).data('allow-clear'))
		});
	});

    //datetimepicker - datum a cas
    $('.datetimepicker').each(function() {
        $(this).datetimepicker({
            language: 'cs',
            format: "dd.mm.yyyy hh:ii",
            todayHighlight: true,
        });
    });
    //datepicker - pouze datum
    $('.datepicker').each(function() {
        $(this).datetimepicker({
            language: 'cs',
            format: "dd.mm.yyyy",
            startView: 2,
            minView: 2,
            maxView: 3,
        });
    });

	//vyhledavac
	$('.chosen-select-ajax-search').select2({
		theme: 'bootstrap4',
		//placeholder: $(this).data('placeholder'),
		placeholder: 'Jméno zákaznice nebo tel. číslo',
		delay: 250,
		ajax: {
			url: $(this).data('link'),
			type: 'GET',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public',
					do: 'searchTerm'
				};
				return query;
			},
			processResults: function (data, params) {
				return {
					results: $.map(data, function(item) {
						return {
							text: item.text,
							id: item.id
						};
					})
				};
			}
		}
	});
    var customerSelect = $('select.select2');
    if (customerSelect.data('customer')) {
        $.ajax({
            type: 'GET',
            url: customerSelect.data('link'),
            data: {
                search: customerSelect.data('customer'),
                type: 'public',
                do: 'getTerm'
            }
        }).then(function (data) {
            var option = new Option(data.text, data.id, true, true);
            customerSelect.append(option).trigger('change');
        });
    }

    //confirmation
    $("a.confirm").on("click", function(event) {
        event.preventDefault();
        bootbox.confirm("Opravdu provést danou akci?", function(result) {
            if (result) {
                window.location = event.target.href;
            }
        });
    });
    //back button
    $("input.btn-back").on("click", function(event) {
        window.location = event.target.getAttribute('link');
    });

})(jQuery); // End of use strict
