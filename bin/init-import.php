<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';
use Nette\Utils\Strings;


$container = App\Bootstrap::boot()
    ->createContainer();

$txtNames = [
    'E:\PM Marta\permanent1.txt',
    'E:\PM Marta\permanent2.txt',
    'E:\PM Marta\permanent3.txt',
];
$txtContacts = [];

$csvName = 'E:\PM Marta\contacts\contacts.csv';
$csvContacts = [];

//nacist csv a vytvorit si pole osob a tel kontaktu
echo "Nacitam kontakty - $csvName \n";

$handle = fopen($csvName, 'r');
while (($data = fgetcsv($handle, 1000, ',')) !== false) {
    $name = getNameFromCsv($data);
    if (empty($name)) {
        continue;
    }
    $numbers = getNumbersFromCsv($data);
    if (empty($numbers)) {
        continue;
    }
    $person = new \stdClass();
    $person->name = $name;
    $person->phones = $numbers;
    $person->note = $data[25]; //Z
    $csvContacts[] = $person;
}
fclose($handle);
array_shift($csvContacts);
echo "Nacteno " . count($csvContacts) . " kontaktu\n";

//nacist txt prevedeny soubor, jmeno dohledat dle tel z csv
foreach ($txtNames as $txtName) {
    echo "Nacitam zaznamy - $txtName \n";

    $handle = fopen($txtName, 'r');
    $person = null;
    $section = null;

    while (($line = fgets($handle)) !== false) {
        $myLine = trim($line, " \t\n\r\0\x0B\x0C!");
        if (empty($myLine)) {
            continue;
        }
        //\Tracy\Debugger::log($myLine);
        if (in_array($myLine, ['Linky', 'Pusa', 'Obočí', 'Poznámky', 'Vertrag', 'Fotky'])) {
            $section = $myLine;
        }

        if ($person && $myLine !== 'INFO') {
            parsePermanentLine($myLine, $section, $person);
        }
        //mam novou osobu
        if ($myLine == 'INFO') {
            //ulozit si pouze kontakty s telefonem a jmenem
            if ($person && $person->phone) {
                $contact = getCsvContactFromPhone($person->phone, $csvContacts);
                if ($contact) {
                    $person->setByContact($contact);
                    $txtContacts[] = $person;
                } else {
                    //mam kontakt ve One Note, ale nemam Google contact
                    $match = Strings::match($txtName, '~\d~');
                    $level = 'mis' . reset($match);
                    \Tracy\Debugger::log($person->phone, $level);
                }
            }
            //pokracuju s novym zaznamem
            $person = new PermanentPerson();

            //if (count($txtContacts) == 2)  break;
        }
        $lastLine = $myLine;
    }
    fclose($handle);
}

die();

$customerRepository = $container->getByType(\App\Model\CustomerRepository::class);
$saleRepository = $container->getByType(\App\Model\SaleRepository::class);

$clear = true;
if ($clear) {
    $saleRepository->deleteAllSales();
    $customerRepository->deleteAllCustomers();
}

foreach ($txtContacts as $contact) {
    //vytvorit zaznam do db
    $data = getCustomerFormData($contact);
    //\Tracy\Debugger::log($data);
    $customer = $customerRepository->saveCustomer($data);
    $sales = getSaleFormData($contact, $customer->id);
    //\Tracy\Debugger::log($sales);

    foreach ($sales as $sale) {
        $saleRepository->saveSale($sale);
    }

}

/********************************FUNKCE**************************************************/
function getCustomerFormData(PermanentPerson $person): \App\Forms\Data\CustomerFormData
{
    $data = new \App\Forms\Data\CustomerFormData();
    $data->phone = $person->phone;
    $data->phone2 = $person->phone2;
    $data->name = $person->firstName;
    $data->surname = $person->lastName;
    $data->gender = 'f';
    $data->note = implode("\n", $person->poznamky);

    $tempSales = getSaleFormData($person, 1);

    $productMap = [];
    /** @var \App\Forms\Data\SaleFormData $tempSales */
    foreach ($tempSales as $sale) {
        foreach ($sale->products as $product) {
            if ($product->product == 1 || $product->product == 6) {
                $productMap[] = 'LD';
            } elseif ($product->product == 2 || $product->product == 7) {
                $productMap[] = 'LH';
            } elseif ($product->product == 3 || $product->product == 8) {
                $productMap[] = 'K';
            } elseif ($product->product == 4 || $product->product == 9) {
                $productMap[] = 'KS';
            } elseif ($product->product == 5 || $product->product == 10) {
                $productMap[] = 'O';
            }
        }
    }
    if (!empty($productMap)) {
        $data->product_map = implode('|', array_unique($productMap));
    }

    return $data;
}

/**
 * @param PermanentPerson $person
 * @param int $customer
 * @return \App\Forms\Data\SaleFormData[]
 */
function getSaleFormData(PermanentPerson $person, int $customer): array
{

    $result = [];
    $lastSale = null;
    foreach ($person->linky as $linky) {
        $myNewSale = parseSale($linky, $customer, 'linky');
        if (!$myNewSale && $lastSale !== null) {
            $result[$lastSale]->note .= ' ' . $linky;
        }
        if (!$myNewSale) {
            continue;
        }

        $result[] = $myNewSale;
        $lastSale = key($result);
    }
    $lastSale = null;
    foreach ($person->pusa as $pusa) {
        $myNewSale = parseSale($pusa, $customer, 'pusa');

        if (!$myNewSale && $lastSale !== null) {
            $result[$lastSale]->note .= ' ' . $pusa;
        }
        if (!$myNewSale) {
            continue;
        }

        $result[] = $myNewSale;
        $lastSale = key($result);
    }
    $lastSale = null;
    foreach ($person->oboci as $oboci) {
        $myNewSale = parseSale($oboci, $customer, 'oboci');
        if (!$myNewSale && $lastSale !== null) {
            $result[$lastSale]->note .= ' ' . $oboci;
        }
        if (!$myNewSale) {
            continue;
        }

        $result[] = $myNewSale;
        $lastSale = key($result);
    }
    return $result;
}

function parseSale(string $line, int $customer, string $product): ?\App\Forms\Data\SaleFormData
{
    $resultData = [
        'customer' => $customer,
        'vertrag' => null,
        'note' => $line,
        'products' => []
    ];
    $mDatum = Strings::match($line, '~\d{1,2}\s?.\d{1,2}\s?.\d{4}~');
    if (!$mDatum) {
        return null;
    }
    $datum = DateTime::createFromFormat('d.m.Y', $mDatum[0]);
    if (!$datum || intval($datum->format('Y')) < 2000 ) {
        return null;
    }
    $resultData['date'] = $datum->format('d.m.Y H:i');

    $tater = null;
    $mTater = Strings::match(Strings::lower($line), '~\b(zuza|zuzka|eva|mirka|mart)~');
    //zuza
    if (!empty($mTater[0]) && Strings::startsWith($mTater[0], 'zuz')) {
        $tater = 4;
    }
    //eva
    if (!empty($mTater[0]) && Strings::startsWith($mTater[0], 'ev')) {
        $tater = 2;
    }
    //mirka
    if (!empty($mTater[0]) && Strings::startsWith($mTater[0], 'mir')) {
        $tater = 5;
    }
    //marta
    if (!empty($mTater[0]) && Strings::startsWith($mTater[0], 'mar')) {
        $tater = 3;
    }
    $oprava = Strings::match($line, '~oprava|opr\.|do roka~');
    if ($product === 'linky') {
        $variants = Strings::matchAll($line, '~LH|LD|\bh-|\bd-~');

        foreach ($variants as $variant) {
            $productId = (in_array($variant[0], ['LH', 'h-'])) ? 1 : 2;
            if ($oprava) {
                $productId = (in_array($variant[0], ['LH', 'h-'])) ? 6 : 7;
            }

            $resultData['products'][] = [
                'sale' => null,
                'product' => $productId,
                'employee' => $tater
            ];
        }
    }
    if ($product === 'pusa') {
        $stin = Strings::match($line, '~stín|NKS|KS~');

        $productId = ($stin) ? 4 : 3;
        if ($oprava) {
            $productId = ($stin) ? 9 : 8;
        }

        $resultData['products'][] = [
            'sale' => null,
            'product' => $productId,
            'employee' => $tater
        ];
    }
    if ($product === 'oboci') {
        $productId = ($oprava) ? 10 : 5;
        $resultData['products'][] = [
            'sale' => null,
            'product' => $productId,
            'employee' => $tater
        ];
    }

    return new \App\Forms\Data\SaleFormData($resultData);
}

function getCsvContactFromPhone(?string $phone, array $contacts): ?\stdClass {
    if (!$phone) {
        return null;
    }
    $number = Strings::substring($phone, -9);
    foreach ($contacts as $contact) {
        foreach ($contact->phones as $cPhone) {
            if (Strings::substring($cPhone, -9) == $number) {
                return $contact;
            }
        }
    }
    return null;
}

/**
 * @param string $myLine
 * @param string|null $section
 * @param PermanentPerson $person
 */
function parsePermanentLine(string $myLine, ?string $section, &$person): void {


    if (Strings::startsWith($myLine, 'Telefon')) {
        //nastavit osobe telefon
        $person->setPhone($myLine);
        return;
    }
    if ($section == 'Pusa') {
        $person->setPusa($myLine);
    }
    if ($section == 'Obočí') {
        $person->setOboci($myLine);
    }
    if ($section == 'Linky') {
        $person->setLinky($myLine);
    }
    if ($section == 'Poznámky') {
        $person->setPoznamky($myLine);
    }
}

function getNumbersFromCsv(array $row): array {
    $result = []; //AE=31, tj. i30 AG=33, tj. i32
    if (!empty($row[30])) {
        foreach (parsePhoneNumberFromCsv($row[30]) as $phone) {
            $result[] = $phone;
        }
    }
    if (empty($row[32])) {
        foreach (parsePhoneNumberFromCsv($row[32]) as $phone) {
            $result[] = $phone;
        }
    }

    return $result;
}

function getNameFromCsv(array $row): ?string {
    if (!empty($row[0])) {
        return $row[0];
    }
    if (!empty($row[3])) {
        return $row[3];
    }
    if (!empty($row[1])) {
        return $row[1];
    }
    return null;
}

function parsePhoneNumberFromCsv(string $dataRow): array {
    $result = [];
    $phones = explode(':::', $dataRow);
    foreach ($phones as $phone) {
        $newPhone = Strings::replace($phone, '~[^\d]~', '');
        if (empty($newPhone)) {
            continue;
        }
        $result[] = $newPhone;
    }
    return $result;
}

class PermanentPerson {

    public $pusa = [];
    public $oboci = [];
    public $linky = [];
    public $poznamky = [];
    public $name = null;
    public $phone = null;
    public $phone2 = null;
    public $firstName = '';
    public $lastName = '';


    public function setPusa(string $pusa): void
    {
        if (empty($pusa) || $pusa == 'Pusa') {
            return;
        }
        $this->pusa[] = $pusa;
    }

    public function setOboci(string $oboci): void
    {
        if (empty($oboci) || $oboci == 'Obočí') {
            return;
        }
        $this->oboci[] = $oboci;
    }


    public function setLinky(string $linky): void
    {
        if (empty($linky) || $linky == 'Linky') {
            return;
        }
        $this->linky[] = $linky;
    }

    public function setPoznamky(string $poznamky): void
    {
        if (empty($poznamky) || $poznamky == 'Poznámky') {
            return;
        }
        $this->poznamky[] = $poznamky;
    }

    /**
     * @param null $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public function setPhone(string $phone): void
    {
        $newPhone = Strings::replace($phone, '~[^\d]~', '');
        if (empty($newPhone)) {
            return;
        }
        $this->phone = $newPhone;
    }

    public function setPhone2(string $phone): void
    {
        $newPhone = Strings::replace($phone, '~[^\d]~', '');
        if (empty($newPhone)) {
            return;
        }
        $this->phone2 = $newPhone;
    }

    public function setByContact(\stdClass $contact): void {
        //nastavit jmeno, nastavit jmeno a prijemni, poznamky, tel 2
        //name, phones, note
        if (!empty($contact->note)) {
            $this->poznamky[] = $contact->note;
        }
        array_unshift($this->poznamky, $contact->name);

        if (empty($this->phone2) && !empty($contact->phones[1])) {
            $this->phone2 = $contact->phones[1];
        }
        $expName = Strings::split($contact->name, '~\s+~');
        foreach ($expName as $key => $eName) {
            if ($key === 0) {
                $this->lastName = $eName;
            } else {
                $this->firstName .= ' ' . $eName;
            }
        }
        $this->firstName = Strings::trim($this->firstName);
    }


}