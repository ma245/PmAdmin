<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';
use Nette\Utils\Strings;


$container = App\Bootstrap::boot()
    ->createContainer();

$txtName = 'E:\PM Marta\Poukazy.txt';


//nacist txt
echo "Nacitam poukazy - $txtName \n";


$voucherType = null;
$voucherTypeMapping = ['CZ Klasické' => 2, 'AT Klasické' => 4, 'CZ Vánoční' => 1, 'AT Vánoční' => 3];
$vouchers = [];

$handle = fopen($txtName, 'r');
while (($line = fgets($handle)) !== false) {
    $myLine = Strings::trim($line);

    //nastavit voucher type
    if (array_key_exists($myLine, $voucherTypeMapping)) {
        $voucherType = $voucherTypeMapping[$myLine];
        continue;
    }

    $voucher = parseVoucher($myLine, $voucherType);
    if ($voucher) {
        $vouchers[] = $voucher;
        //break;
    }
}
fclose($handle);
echo "Nacteno " . count($vouchers) . " poukazu\n";


$voucherRepository = $container->getByType(\App\Model\VoucherRepository::class);
$voucherRepository->deleteAllVouchers();
/**
 * @var \App\Forms\Data\VoucherFormData $voucher
 */
foreach ($vouchers as $voucher) {
    \Tracy\Debugger::log($voucher->serialize());
    if (empty($voucher->dateOrder)) {
        $voucher->dateOrder = '01.01.2020';
    }


    $voucherRepository->save($voucher);
}




function parseVoucher(string $line, int $type)
{
    $myLine = Strings::replace(Strings::trim($line), '~\s{2,}~', '|');

    $explode = explode('|', $myLine);
    if (count($explode) <= 1) {
        return null;
    }
    if ($explode[0] == '# ČÍSLO' || $explode[0] == 'ZAKOUPENÍ') {
        return null;
    }

    if (!is_numeric($explode[0])) {
        return null;
    }
    $voucher = new \App\Forms\Data\VoucherFormData();
    $voucher->type = $type;
    $voucher->number = intval($explode[0]);
    $voucher->name = $explode[1];

    $value = Strings::match($explode[2], '~\d+~');
    $voucher->value = intval($value[0]);

    $currency = Strings::match($explode[2], '~\D+~');
    if (Strings::contains($currency[0], 'K')) {
        $voucher->currency = 'CZK';
    }
    if (Strings::contains($currency[0], 'EUR') || Strings::contains($currency[0], '€')) {
        $voucher->currency = 'EUR';
    }
    //produkt
    $voucher->product = $explode[3];
    //datum zakoupeni
    if (array_key_exists(4, $explode) && !empty($explode[4])) {
        $date = \Nette\Utils\DateTime::createFromFormat('d.m.Y', $explode[4]);
        if ($date) {
            $voucher->dateOrder = $date->format('d.m.Y');
            $valid = clone $date;
            $valid->modify('+1 year');
            $voucher->dateValidity = $valid->format('d.m.Y');
        }
    }

    //datum cerpani
    if (array_key_exists(5, $explode) && !empty($explode[5])) {
        $date = \Nette\Utils\DateTime::createFromFormat('d.m.Y', $explode[5]);
        if ($date) {
            $voucher->dateUse = $date->format('d.m.Y');
        }
    }

    //datum vyrizeni
    if (array_key_exists(6, $explode) && !empty($explode[6])) {
        $date = \Nette\Utils\DateTime::createFromFormat('d.m.Y', $explode[6]);
        if ($date) {
            $voucher->dateConfirm = $date->format('d.m.Y');
        }
    }

    return $voucher;
}

