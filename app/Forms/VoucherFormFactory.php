<?php
namespace App\Forms;

use App\Forms\Data\VoucherFormData;
use App\Model\VoucherRepository;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;

class VoucherFormFactory {

    private VoucherRepository $repository;

    public function __construct(VoucherRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(): Form {
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];

        $form->setMappedType(VoucherFormData::class);

		$form->addHidden('id', null);
        //TYP
        $form->addSelect('type', 'Typ', $this->findVoucherTypeFormItem())
            ->setRequired();
        //CISLO
        $form->addInteger('number', 'Číslo')
            ->setRequired();
        //NAZEV
        $form->addText('name', 'Název')
            ->setRequired();
        //MENA
        $currencyItems = [
            'EUR' => 'EUR',
            'CZK' => 'CZK'
        ];
        $form->addSelect('currency', 'Měna', $currencyItems)
            ->setRequired();
        //HODNOTA
        $form->addInteger('value', 'Hodnota')
            ->setRequired();
        //PRODUKT
        $form->addText('product', 'Produkt');
        //EMAIL
        $form->addText('email', 'Email');

        //DATUM OBJEDNANI
        $form->addText('date_order', 'Datum zakoupení')
            ->setHtmlAttribute('class','datepicker');
        //DATUM VYRIZENI
        $form->addText('date_confirm', 'Datum vyřízení')
            ->setHtmlAttribute('class','datepicker');
        //DATUM PLATNOSTI
        $form->addText('date_validity', 'Datum platnosti')
            ->setHtmlAttribute('class','datepicker');
        //DATUM POUZITI
        $form->addText('date_use', 'Datum použití')
            ->setHtmlAttribute('class','datepicker');

        //POZNAMKA
        $form->addTextArea('note', 'Poznámka');
        //ULOZIT
		$form->addSubmit('send', 'Uložit');
		
		$form->onValidate[] = [$this, 'validateForm'];
		$form->onSuccess[] = [$this, 'processForm'];

		return $form;
	}

	
	public function validateForm(Form $form, VoucherFormData $data): void
    {
		$errors = $data->validate();
		foreach ($errors as $error) {
			$form->addError($error);
		}
	}
	
	public function processForm(Form $form, VoucherFormData $values): void
    {
		$this->repository->save($values);
	}

    private function findVoucherTypeFormItem(): array
    {
        $result = [];
        $rows = $this->repository->findVoucherType();
        foreach ($rows as $row) {
            $itemName = $row->name;
            if (!empty($row->note)) {
                $itemName .= ' (' . Strings::truncate($row->note, 30) . ')';
            }
            $result[$row->id] = $itemName;
        }

        return $result;
    }
}
