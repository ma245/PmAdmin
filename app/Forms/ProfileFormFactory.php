<?php
namespace App\Forms;

use App\Forms\Data\EmployeeFormData;
use Nette\Application\UI\Form;
use Nette\Utils\Validators;
use App\Model\EmpRepository;
use Tracy\Debugger;

class ProfileFormFactory {

	private EmpRepository $empRepository;
	
	public function __construct(EmpRepository $repository) {
		$this->empRepository = $repository;
	}
	
	public function create(): Form {
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];
		$form->addHidden('id');

		$form->addText('name', 'Jméno')
			->setRequired();

		$form->addText('surname', 'Příjmení')
			->setRequired();

		$form->addText('phone', 'Telefon');
		
		$form->addEmail('email', 'Email')
			->setRequired();

        $form->addMultiSelect('post', 'Funkce', EmployeeFormData::getEmployeePost());

        $form->addSelect('role', 'Role', EmployeeFormData::getEmployeeRole());

		$form->addSubmit('send', 'Uložit');
		
		$form->onValidate[] = [$this, 'validateForm'];
		$form->onSuccess[] = [$this, 'processForm'];

		return $form;
	}
	
	public function validateForm(Form $form, EmployeeFormData $data) {
        $errors = $data->validate();
        foreach ($errors as $error) {
            $form->addError($error);
        }
	}
	
	public function processForm(Form $form, EmployeeFormData $values) {
        $this->empRepository->saveEmployee($values);
	}
}
