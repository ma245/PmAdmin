<?php
namespace App\Forms\Data;

use Nette\Utils\Strings;

class VoucherOrderFormData {

    public $id;
    public ?string $name;
    public string $phone;
    public string $post;
    public ?string $address;
    public ?string $email;

	
	public function validate(): array
    {
		$errors = [];

		if (empty($this->phone)) {
            $errors[] = 'messages.voucherFormPhoneEmpty';
        }
        if (!empty($this->phone)) {
            $phone = Strings::replace($this->phone, '~\s+~');
            if (!Strings::match($phone, '~\+?\d{9,20}~')) {
                $errors[] = 'messages.voucherFormPhoneNonValid';
            }
        }
		
		return $errors;
	}
	
	public function serialize(): array
    {
		return [
			'id'		=> $this->id,
			'name'      => $this->name,
            'phone'     => Strings::replace($this->phone, '~\s+~'),
            'sendPost'  => ($this->post == 'post'),
            'sendMail'  => ($this->post == 'email'),
            'email'     => empty($this->email) ? null : $this->email,
            'address'   => empty($this->address) ? null : $this->address,
		];
	}

}
