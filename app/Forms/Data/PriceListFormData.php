<?php
namespace App\Forms\Data;

use Nette\Utils\Validators;
use Nette\Utils\DateTime;

class PriceListFormData {

    public $id;
    public string $amount_czk;
    public string $amount_eur;
    public string $valid_from;
    public int $product;

	
	public function validate(): array {
		$errors = [];

		if (!Validators::isNumeric($this->amount_czk) ) {
			$errors[] = 'Castka CZK';
		}
        if (!Validators::isNumeric($this->amount_eur)) {
            $errors[] = 'Castka EUR';
        }

        $date = DateTime::createFromFormat('d.m.Y', $this->valid_from);
        if ($date === false) {
            $errors[] = 'Spatny format Planost od';
        }
		
		return $errors;
	}
	
	public function serialize() {
		return [
			'id'		    => $this->id,
			'valid_from'    => DateTime::createFromFormat('d.m.Y', $this->valid_from),
			'amount_czk'    => $this->amount_czk,
            'amount_eur'    => $this->amount_eur,
            'product'       => $this->product,
		];
	}


}
