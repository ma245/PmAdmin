<?php
namespace App\Forms\Data;

use Nette\Utils\Validators;
use Tracy\Debugger;

class EmployeePasswordFormData {
	
	public $id;
    public ?string $password_old;
    public string $password;
	public string $password2;
	
	public function validate(): array
    {
		$errors = [];

        if (empty($this->password) || empty($this->password2)) {
            $errors[] = 'Nebyly vyplněny všechny povinné položky';
        }

        $minLength = EmployeeFormData::EMP_PWD_LENGTH;
        if (!Validators::is($this->password, "string:$minLength..")) {
            $errors[] = "Nové heslo musím mít $minLength minimálně znaků";
        }

        if ($this->password !== $this->password2) {
            $errors[] = "Nové heslo a heslo pro kontrolu se musí shodovat.";
        }

		return $errors;
	}
	
	public function serialize(): array
    {
		return [
			'id'	    => $this->id,
			'password'  => $this->password
		];
	}

}
