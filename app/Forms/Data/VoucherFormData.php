<?php
namespace App\Forms\Data;


use Nette\Utils\DateTime;
use Tracy\Debugger;

class VoucherFormData {

    public $id;
    public int $number;
    public string $name;
    public string $currency;
    public float $value;
    public string $product;
    public ?string $email;
    public ?string $note;
    public $type;
    public ?string $date_order;
    public ?string $date_use;
    public ?string $date_confirm;
    public ?string $date_validity;


	public function validate(): array
    {
		$errors = [];

		if (empty($this->name)) {
            $errors[] = 'Název je povinna polozka a nesmi byt prazdna';
        }
		
		return $errors;
	}
	
	public function serialize(): array
    {
        $dateOrder = isset($this->date_order) ? DateTime::createFromFormat('d.m.Y', $this->date_order) : null;
        $dateUse = isset($this->date_use) ? DateTime::createFromFormat('d.m.Y', $this->date_use) : null;
        $dateConfirm = isset($this->date_confirm) ? DateTime::createFromFormat('d.m.Y', $this->date_confirm) : null;
        $dateValidity = isset($this->date_validity) ? DateTime::createFromFormat('d.m.Y', $this->date_validity) : null;

		return [
			'id'		=> $this->id,
			'name'      => $this->name,
            'number'    => $this->number,
            'currency'  => $this->currency,
            'value'     => $this->value,
            'product'   => $this->product,
            'email'     => (empty($this->email)) ? null : $this->email,
            'date_order'=> ($dateOrder) ? $dateOrder : null,
            'date_use'  => ($dateUse) ? $dateUse : null,
            'date_confirm'  => ($dateConfirm) ? $dateConfirm : null,
            'date_validity' => ($dateValidity) ? $dateValidity : null,
            'note'      => (empty($this->note)) ? null : $this->note,
            'type'      => intval($this->type)
		];
	}

}
