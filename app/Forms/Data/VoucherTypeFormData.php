<?php
namespace App\Forms\Data;


class VoucherTypeFormData {

    public $id;
    public string $name;
    public ?string $note;

	public function validate(): array
    {
		$errors = [];

		if (empty($this->name)) {
            $errors[] = 'Název je povinna polozka a nesmi byt prazdna';
        }
		
		return $errors;
	}
	
	public function serialize(): array
    {
		return [
			'id'		=> $this->id,
			'name'      => $this->name,
            'note'      => (empty($this->note)) ? null : $this->note
		];
	}

}
