<?php
namespace App\Forms\Data;

use Nette\Http\FileUpload;
use Nette\Utils\DateTime;
use Nette\Utils\Validators;
use Tracy\Debugger;

class FileFormData {
	const FILE_TYPE_CONTRACT = 'vertrag';
    const FILE_TYPE_PHOTO = 'photo';
}
