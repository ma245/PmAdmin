<?php
namespace App\Forms\Data;

use Nette\Utils\Validators;

class CustomerFormData {
	
	public $id;
	public string $name;
	public string $surname;
	public string $phone;
	public ?string $phone2;
	public string $gender;
	public ?string $note;
    public ?string $product_map = null;
	
	public function validate() {
		$errors = [];
		if (!Validators::is($this->name, 'string:2..')) {
			$errors[] = 'Jmeno je spatne';
		}
		if (!Validators::is($this->surname, 'string:2..')) {
			$errors[] = 'Prijmeni je spatne';
		}
		if (!Validators::is($this->phone, 'string:8..30')) {
			$errors[] = 'Tel je spatne';
		}
		if (!empty($this->phone2) && !Validators::is($this->phone, 'string:8..30')) {
			$errors[] = 'Tel2 je spatne';
		}
		if (!Validators::is($this->gender, 'string:1') || !in_array($this->gender, ['f', 'm'])) {
			$errors[] = 'Pohlavi';
		}
		return $errors;
	}
	
	public function __serialize() {
		return [
			'id'	        => $this->id,
			'name'			=> $this->name,
			'surname'		=> $this->surname,
			'phone'			=> $this->phone,
			'phone2'		=> $this->phone2,
			'gender'		=> $this->gender,
			'note'			=> $this->note,
            'product_map'   => $this->product_map
		];
	}

}
