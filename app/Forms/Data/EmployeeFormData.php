<?php
namespace App\Forms\Data;

use Nette\Utils\Validators;

class EmployeeFormData {
	
	public $id;
	public string $name;
    public string $surname;
	public ?string $phone;
	public string $email;
    public string $password;
    public string $role;
    public array $post;

    const EMP_DEF_PWD = 'hesloPM';
    const EMP_PWD_LENGTH = 5;
	
	public function validate(): array
    {
		$errors = [];
		if (!Validators::is($this->name, 'string:2..')) {
			$errors[] = 'Jmeno musi obsahovat minimalne dva znaky';
		}
		if (!Validators::is($this->surname, 'string:2..')) {
			$errors[] = 'Prijmeni musi obsahovat minimalne dva znaky';
		}
		if (!Validators::isEmail($this->email)) {
            $errors[] = 'Neplatny email';
        }
		return $errors;
	}
	
	public function serialize()
    {
		return [
			'id'	    => $this->id,
			'name'	    => $this->name,
            'surname'   => $this->surname,
            'phone'     => $this->phone,
            'email'     => $this->email,
            'role'      => json_encode([$this->role]),
            'post'      => json_encode($this->post),
		];
	}

    public static function getEmployeePost(): array
    {
        return [
            'ADMIN' => 'Správce',
            'OWNER' => 'Vlastník',
            'TATER' => 'Tatér',
            'RECEPTIONIST'  => 'Recepční'
        ];
    }

    public static function getEmployeeRole(): array {
        return [
            'USER'  => 'USER',
            'ADMIN' => 'ADMIN'
        ];
    }

}
