<?php
namespace App\Forms\Data;

use Nette\Utils\Validators;

class ProductFormData {
	
	public $id;
	public string $name;
	public string $abbrev;
    public string $description;
	public int $duration;
	public bool $repair;
	
	public function validate(): array {
		$errors = [];
		if (!Validators::is($this->name, 'string:2..')) {
			$errors[] = 'Nazev musi obsahovat minimalne dva znaky';
		}
		if (!Validators::is($this->abbrev, 'string:2..')) {
			$errors[] = 'Zkratka musi obsahovat minimalne dva znaky';
		}
		if (!Validators::is($this->duration, 'int:1..200')) {
			$errors[] = 'Cas je spatne';
		}
		return $errors;
	}
	
	public function serialize() {
		return [
			'id'	    => $this->id,
			'name'	    => $this->name,
            'abbrev'    => $this->abbrev,
            'description' => $this->description,
            'duration'  => $this->duration,
            'repair'    => $this->repair
		];
	}

}
