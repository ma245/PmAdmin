<?php
namespace App\Forms\Data;

use Nette\Http\FileUpload;
use Nette\Utils\DateTime;
use Nette\Utils\Validators;
use Tracy\Debugger;

class SaleFormData {
	
	public $id;
	public int $customer;
	public string $date;
	public ?string $note;
    public ?FileUpload $vertrag;
    /** @var SaleProductFormData[] */
    public array $products;

    public function __construct(?array $formData)
    {
        $this->id = intval($formData['id']);
        $this->customer = $formData['customer'];
        $this->date = $formData['date'];
        $this->note = $formData['note'];
        $this->vertrag = $formData['vertrag'];
        $this->products = [];
        foreach ($formData['products'] as $product) {
            $this->products[] = new SaleProductFormData($product);
        }
    }

    public function validate(): array
    {
		$errors = [];
        $date = DateTime::createFromFormat('d.m.Y H:i', $this->date);
        if (!$date) {
            $errors[] = 'Chybne datum';
        }

		return $errors;
	}
	
	public function serialize(): array
    {
		return [
			'id'	    => $this->id,
			'customer'	=> $this->customer,
			'date'		=> DateTime::createFromFormat('d.m.Y H:i', $this->date),
			'note'		=> $this->note,
		];
	}

    /**
     * @return SaleProductFormData[]
     */
    public function serializeProducts(): array
    {
        foreach ($this->products as $product) {
            $product->sale = $this->id;
        }
        return $this->products;
    }

}
