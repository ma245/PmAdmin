<?php
namespace App\Forms\Data;

use Nette\Utils\Validators;
use Nette\Utils\DateTime;

class OrderFormData {

    public $id;
    public string $date;
    public string $name;
    public string $phone;
    public array $products;
    public int $state;
    public ?string $note;

	
	public function validate(): array {
		$errors = [];

		if (!Validators::isInRange($this->state, [1, 4])) {
			$errors[] = 'Stav ';
		}
        //validace stavu - prechodu


        $date = DateTime::createFromFormat('d.m.Y H:i', $this->date);
        if ($date === false) {
            $errors[] = 'Spatny format Terminu';
        }
		
		return $errors;
	}
	
	public function serialize() {
		return [
			'id'		=> $this->id,
			'date'		=> DateTime::createFromFormat('d.m.Y H:i', $this->date),
			'name'      => $this->name,
            'phone'     => $this->phone,
            'state'     => $this->state,
            'note'      => $this->note,
		];
	}
	
	public static function getOrderStates(): array {
		return [
			1 => 'Nová',
			2 => 'Potvrzená',
			3 => 'Realizovaná',
			4 => 'Zrušená',
		];
	}
	
	public static function getOrderStatesByState(?int $state = 0): array {
		$states = self::getOrderStates();
		//mapovani prechodu stavu objednavky
		$mapping = [
			0 => [1],           //zalozeni Nove objednavky
			1 => [2, 3, 4],     //Nova => Potvrzena, Realizovana, Zrusena
			2 => [3, 4],        //Potvrzena => Realizovana, Zrusena
			3 => [],            //Realizovana => null
			4 =>  [1, 2]        //Zrusena => Nova, Potvrzena
		];
		$pStates = $mapping[$state];
		$result = [];
		foreach ($pStates as $state) {
			$result[$state] = $states[$state];
		}
		return $result;
	}
	


}
