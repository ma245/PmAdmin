<?php
namespace App\Forms\Data;

use Nette\Http\FileUpload;
use Nette\Utils\DateTime;
use Nette\Utils\Validators;
use Tracy\Debugger;

class SaleProductFormData {
	
	public $id;
	public ?int $sale;
	public ?int $product;
	public ?int $employee;

    public function __construct(?array $formData)
    {
        //obsahuje tri klice> sale_product_id, product a employee
        $this->id = $formData['sale_product_id'];
        $this->sale = array_key_exists('sale', $formData) ? $formData['sale'] : null;
        $this->product = $formData['product'];
        $this->employee = $formData['employee'];

    }
	
	public function serialize(): array
    {
		return [
			'id'	    => $this->id,
			'sale'	    => $this->sale,
			'product'	=> $this->product,
			'employee'	=> $this->employee,
		];
	}

}
