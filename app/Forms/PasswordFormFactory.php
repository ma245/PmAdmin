<?php
namespace App\Forms;

use App\Forms\Data\EmployeePasswordFormData;
use Nette\Application\UI\Form;
use App\Model\EmpRepository;
use Nette\Security\Passwords;

class PasswordFormFactory
{

	private EmpRepository $empRepository;
    private Passwords $passwords;
	
	public function __construct(EmpRepository $repository, Passwords $passwords)
    {
		$this->empRepository = $repository;
        $this->passwords = $passwords;
	}
	
	public function create(): Form
    {
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];
		
		$form->addHidden('id');
		
		$form->addPassword('password_old', 'Původní heslo')
			->setRequired()
			->setHtmlAttribute('autocomplete', 'new-password');
		
		$form->addPassword('password', 'Nové heslo')
			->setRequired()
			->setHtmlAttribute('autocomplete', 'new-password');
		
		$form->addPassword('password2', 'Nové heslo pro kontrolu')
			->setRequired()
			->setHtmlAttribute('autocomplete', 'new-password');
		
		$form->addSubmit('send', 'Uložit');
		
		$form->onValidate[] = [$this, 'validateForm'];
		$form->onSuccess[] = [$this, 'processForm'];

		return $form;
	}
	
	public function validateForm(Form $form, EmployeePasswordFormData $values): void
    {
	    //obecna validace
        $errors = $values->validate();
        foreach ($errors as $error) {
            $form->addError($error);
        }
        //specificka validace
		if (empty($values->password_old)) {
			$form->addError('Nebyly vyplněny všechny povinné položky');
		}

		if (!$this->validateOldPassword($values->id, $values->password_old)) {
			$form->addError("Nepodařilo se ověřit původní heslo.");
		}
	}
	
	public function processForm(Form $form, EmployeePasswordFormData $values): void
    {
		$this->empRepository->editEmpPwd($values);
	}
	
	private function validateOldPassword(int $id, string $password): bool
    {
		$emp = $this->empRepository->getEmp($id);
		if (!$emp) {
			return false;
		}
		return $this->passwords->verify($password, $emp->password);
	}
}
