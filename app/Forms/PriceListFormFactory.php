<?php
namespace App\Forms;

use App\Forms\Data\PriceListFormData;
use App\Model\PriceListRepository;
use App\Model\ProductRepository;
use Nette\Application\UI\Form;
use App\Forms\Data\OrderFormData;
use Tracy\Debugger;

class PriceListFormFactory {

	private ProductRepository $productRepository;
    private PriceListRepository $priceListRepository;
	
	public function __construct(PriceListRepository $priceListRepository, ProductRepository $productRepository) {
		$this->productRepository = $productRepository;
        $this->priceListRepository = $priceListRepository;
	}
	
	public function create(): Form {
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];
		$form->setMappedType('App\Forms\Data\PriceListFormData');
		$form->addHidden('id', null);

        //PRODUKT
        $products = $this->getProductItems();
        $form->addSelect('product', 'Procedura', $products);

        //PLATNOST OD
		$form->addText('valid_from', 'Platnost od')
            ->setHtmlAttribute('class','datepicker')
			->setRequired();

        //castka CZK
        $form->addText('amount_czk', 'Částka Kč');
        //castka EUR
        $form->addText('amount_eur', 'Částka EUR');

		$form->addSubmit('send', 'Uložit');
		
		$form->onValidate[] = [$this, 'validateForm'];
		$form->onSuccess[] = [$this, 'processForm'];

		return $form;
	}
	
	protected function getProductItems(): array {
		$result = [];
		$productRows = $this->productRepository->findProducts();
		foreach ($productRows as $product) {
			$result[$product->id] = $product->name;
		}
		return $result;
	}
	
	public function validateForm(Form $form, PriceListFormData $data) {
		$errors = $data->validate();
		foreach ($errors as $error) {
			$form->addError($error);
		}
	}
	
	public function processForm(Form $form, PriceListFormData $values) {
		$this->priceListRepository->savePriceList($values);
	}
}
