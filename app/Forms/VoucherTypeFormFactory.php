<?php
namespace App\Forms;

use App\Forms\Data\ProductFormData;
use App\Forms\Data\VoucherTypeFormData;
use App\Model\VoucherTypeRepository;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class VoucherTypeFormFactory {

    private VoucherTypeRepository $repository;

    public function __construct(VoucherTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(): Form {
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];

        $form->setMappedType(VoucherTypeFormData::class);

		$form->addHidden('id', null);

        //NAZEV
        $form->addText('name', 'Název');
        //ZKRATKA
        $form->addTextArea('note', 'Poznámka');
        //ULOZIT
		$form->addSubmit('send', 'Uložit');
		
		$form->onValidate[] = [$this, 'validateForm'];
		$form->onSuccess[] = [$this, 'processForm'];

		return $form;
	}

	
	public function validateForm(Form $form, VoucherTypeFormData $data): void
    {
		$errors = $data->validate();
		foreach ($errors as $error) {
			$form->addError($error);
		}
	}
	
	public function processForm(Form $form, VoucherTypeFormData $values) {
		$this->repository->save($values);
	}
}
