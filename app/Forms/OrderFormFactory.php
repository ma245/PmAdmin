<?php
namespace App\Forms;

use App\Model\OrderRepository;
use App\Model\ProductRepository;
use Nette\Application\UI\Form;
use App\Forms\Data\OrderFormData;
use Tracy\Debugger;

class OrderFormFactory {

	private ProductRepository $productRepository;
    private OrderRepository $orderRepository;
	
	public function __construct(OrderRepository $orderRepository, ProductRepository $productRepository) {
		$this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
	}
	
	public function create(): Form {
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];
		$form->setMappedType('App\Forms\Data\OrderFormData');
		$form->addHidden('id', null);

        //JMENO ZAKAZNICE
        $form->addText('name', 'Jméno')
            ->setRequired();

        //TELEFON ZAKAZNICE
        $form->addText('phone', 'Telefon');

        //TERMIN
		$form->addText('date', 'Termín')
            ->setHtmlAttribute('class','datetimepicker')
			->setRequired();
		/*
		$form->addSelect('customer', 'Zákazník')
			->setHtmlAttribute('class', 'chosen-select-ajax')
			->setHtmlAttribute('data-placeholder', 'Vyber zákaznici podle jména nebo tel. čísla')
			->checkDefaultValue(false);
        */
		$products = $this->getProductItems();
		$form->addMultiSelect('products', 'Procedura', $products)
			->setHtmlAttribute('class', 'chosen-select')
			->setHtmlAttribute('data-placeholder', 'Vyber objednané procedury');

		$form->addTextArea('note', 'Poznámka');
		
		$states = OrderFormData::getOrderStates();
		$form->addSelect('state', 'Stav', $states)
			->setRequired();

		$form->addSubmit('send', 'Uložit');
		
		$form->onValidate[] = [$this, 'validateForm'];
		$form->onSuccess[] = [$this, 'processForm'];

		return $form;
	}
	
	protected function getProductItems(): array {
		$result = [];
		$productRows = $this->productRepository->findProducts();
		foreach ($productRows as $product) {
			$result[$product->id] = $product->name;
		}
		return $result;
	}
	
	public function validateForm(Form $form, OrderFormData $data) {
		$errors = $data->validate();
		foreach ($errors as $error) {
			$form->addError($error);
		}
	}
	
	public function processForm(Form $form, OrderFormData $values) {
		$this->orderRepository->saveOrder($values);
	}
}
