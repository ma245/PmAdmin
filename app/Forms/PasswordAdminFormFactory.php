<?php
namespace App\Forms;

use App\Forms\Data\EmployeePasswordFormData;
use Nette\Application\UI\Form;
use App\Model\EmpRepository;
use Tracy\Debugger;

class PasswordAdminFormFactory
{

	private EmpRepository $empRepository;
	
	public function __construct(EmpRepository $repository)
    {
		$this->empRepository = $repository;
	}
	
	public function create(): Form
    {
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];
		
		$form->addHidden('id');
		
		$form->addPassword('password', 'Nové heslo')
			->setRequired()
			->setHtmlAttribute('autocomplete', 'new-password');
		
		$form->addPassword('password2', 'Nové heslo pro kontrolu')
			->setRequired()
			->setHtmlAttribute('autocomplete', 'new-password');
		
		$form->addSubmit('send', 'Uložit');
		
		$form->onValidate[] = [$this, 'validateForm'];
		$form->onSuccess[] = [$this, 'processForm'];

		return $form;
	}
	
	public function validateForm(Form $form, EmployeePasswordFormData $values): void
    {

		$errors = $values->validate();
        foreach ($errors as $error) {
            $form->addError($error);
        }

	}
	
	public function processForm(Form $form, EmployeePasswordFormData $values): void
    {
		$this->empRepository->editEmpPwd($values);
	}

}
