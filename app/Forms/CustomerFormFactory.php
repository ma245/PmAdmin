<?php
declare(strict_types=1);

namespace App\Forms;

use Nette\Application\UI\Form;
use App\Model\CustomerRepository;
use App\Forms\Data\CustomerFormData;

class CustomerFormFactory
{

	private CustomerRepository $customerRepository;
	
	public function __construct(CustomerRepository $repository)
    {
		$this->customerRepository = $repository;
	}
	
	public function create(): Form
    {
		$sex = [
			'f' => 'žena',
			'm' => 'muž'
		];
		
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];
		$form->setMappedType('App\Forms\Data\CustomerFormData');
		
		$form->addHidden('id');
		$form->addText('name', 'Jméno');
		$form->addText('surname', 'Příjmení');
		$form->addText('phone', 'Telefon');
		$form->addText('phone2', 'Telefon 2');
		$form->addRadioList('gender', 'Pohlaví', $sex)
			->setDefaultValue('f');
		$form->addTextArea('note', 'Poznámka', 10, 5);
		
		$form->addSubmit('send', 'Uložit');
		$form->onValidate[] = [$this, 'formValidate'];
		$form->onSuccess[] = [$this, 'formSuccess'];

		return $form;
	}
	
	public function formValidate(Form $form, CustomerFormData $data)
    {
		$errors = $data->validate();
		foreach ($errors as $error) {
			$form->addError($error);
		}
	}
	
	public function formSuccess(Form $form, CustomerFormData $data)
    {
		$this->customerRepository->saveCustomer($data);
	}
}
