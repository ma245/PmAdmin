<?php
namespace App\Forms;

use App\Forms\Data\VoucherOrderFormData;
use App\Model\VoucherOrderRepository;
use Nette\Application\UI\Form;
use Nette\Mail\Mailer;
use Nette\Mail\Message;
use Nette\Localization\Translator;

class VoucherForm {

	protected Mailer $mailer;
	protected Translator $translator;
    protected VoucherOrderRepository $repository;

	public function __construct(Mailer $mailer,
                                Translator $translator,
                                VoucherOrderRepository $repository)
    {
		$this->mailer = $mailer;
		$this->translator = $translator;
        $this->repository = $repository;
	}
	
	public function create(): Form
    {
		$form = new Form();
		$form->setTranslator($this->translator);
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];

		$form->addText('name', 'messages.name')
				->setHtmlAttribute('placeholder', 'messages.name')
				->setMaxLength(100);
		
		$form->addText('phone', 'messages.phoneNumber')
				->setHtmlAttribute('placeholder', 'messages.phoneNumber')
				->setRequired('messages.voucherFormPhoneEmpty')
				//->addRule($form::PATTERN_ICASE, 'messages.voucherFormPhoneNonValid', '\+?\d{9,20}')
				->setMaxLength(50);
		
		$form->addSelect('post', 'messages.voucherPostBy', ['email' => 'messages.voucherPostByM', 'post' => 'messages.voucherPostByP'])
				->addCondition($form::EQUAL, 'post')
					->toggle('address-address')
				->endCondition()
				->addCondition($form::EQUAL, 'email')
					->toggle('email-email');
		
		$form->addText('email', 'Email')
				->setOption('id', 'email-email')
				->setHtmlAttribute('placeholder', 'Email')
				->addRule($form::EMAIL, 'messages.voucherFormMailNonValid')
				->setMaxLength(50);
		
		$form->addText('address', 'messages.voucherAddress')
				->setOption('id', 'address-address')
				->setHtmlAttribute('placeholder', 'messages.voucherAddress')
				->setMaxLength(100);
		
		$form->addSubmit('send', 'messages.send');
		
		$form->onValidate[]	= [$this, 'validate'];
        $form->onSuccess[]	= [$this, 'save'];
		$form->onSuccess[]	= [$this, 'sendMail'];

		return $form;
	}
	
	public function validate(Form $form, VoucherOrderFormData $values)
	{
        $errors = $values->validate();
		foreach ($errors as $error) {
            $form->addError($error);
        }
		
	}

    public function save(Form $form, VoucherOrderFormData $values): void
    {
        $this->repository->saveVoucherWebOrder($values);
    }

	public function sendMail(Form $form, \stdClass $values): void
	{
		$body = "Nová poptávka z webu. Kontakt {$values->phone}, jméno {$values->name}, má zájem o Voucher\n";
		if ($values->post == 'email') {
			$isEmailValid = filter_var($values->email, FILTER_VALIDATE_EMAIL);
			$body .= "Voucher zaslat emailem na: {$values->email}";
			if (!$isEmailValid) {
				$body .= " Pozor, email není validní, musíte si vykomunikovat jeho opravu";
			}
		} else if ($values->post == 'post') {
            $body .= "Voucher zaslat poštou na: {$values->address}";
        }

		$mail = new Message;
		$mail->setFrom('postmaster@pmmarta.eu', 'PM Marta')
			->addTo('info@pmmarta.eu')
			//->addTo('krupaj94@gmail.com')
			->setSubject('[WEB]: Voucher')
			->setBody($body);
		$this->mailer->send($mail);
		
	}
	
}
