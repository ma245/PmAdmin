<?php
namespace App\Forms;

use App\Forms\Data\ProductFormData;
use App\Model\ProductRepository;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class ProductFormFactory {

    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function create(): Form {
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];
		$form->setMappedType(ProductFormData::class);
		$form->addHidden('id', null);

        //NAZEV
        $form->addText('name', 'Název');
        //ZKRATKA
        $form->addText('abbrev', 'Zkratka', null, 10);
        //POPIS
        $form->addTextArea('description', 'Popis');
        //DOBA TRVANI
        $form->addInteger('duration', 'Doba trvání (minuty)');
        //JEDNA SE O OPRAVU
        $form->addCheckbox('repair', 'Oprava?');
        //ULOZIT
		$form->addSubmit('send', 'Uložit');
		
		$form->onValidate[] = [$this, 'validateForm'];
		$form->onSuccess[] = [$this, 'processForm'];

		return $form;
	}

	
	public function validateForm(Form $form, ProductFormData $data) {
		$errors = $data->validate();
		foreach ($errors as $error) {
			$form->addError($error);
		}
	}
	
	public function processForm(Form $form, ProductFormData $values) {
		$this->productRepository->saveProduct($values);
	}
}
