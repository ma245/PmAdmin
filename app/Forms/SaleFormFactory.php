<?php
declare(strict_types=1);

namespace App\Forms;

use App\Forms\Data\SaleFormData;
use App\Model\EmpRepository;
use App\Model\ProductRepository;
use App\Model\SaleRepository;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;
use Nette\Forms\Container;
use Tracy\Debugger;

class SaleFormFactory
{
	private SaleRepository $saleRepository;
    private ProductRepository $productRepository;
    private EmpRepository $empRepository;
    private ?ActiveRow $row;
	
	public function __construct(
        SaleRepository $saleRepository,
        ProductRepository $productRepository,
        EmpRepository $empRepository)
    {
		$this->saleRepository = $saleRepository;
        $this->productRepository = $productRepository;
        $this->empRepository = $empRepository;
	}
	
	public function create(?ActiveRow $row): Form
    {
		$form = new Form();
		$form->onRender[] = [BootstrapForm::class, 'makeBootstrap4'];
		
		$form->addHidden('id');

        //ZAKAZNIK
        $form->addSelect('customer', 'Zákazník')
            ->setHtmlAttribute('class', 'chosen-select-ajax-search select2')
            ->setHtmlAttribute('data-placeholder', 'Jméno zákaznice nebo tel. číslo')
            ->checkDefaultValue(false);
        if ($row) {
            $form['customer']->setHtmlAttribute('data-customer', $row->customer);
        }

        //TERMIN
        $form->addText('date', 'Termín')
            ->setHtmlAttribute('class','datetimepicker')
            ->setRequired();

        //POZNAMKA
		$form->addTextArea('note', 'Poznámka', 10, 5);

        //SMLOUVA
        $form->addUpload('vertrag', 'Vertrag');

        //PROCEDURA
        $productMulti = $form->addMultiplier('products', function(Container $container, \Nette\Forms\Form $form) {
            $itemProduct = $this->getProductItems();
            $itemEmployee = $this->getEmpItems();
            $container->addSelect('product', 'Procedura', $itemProduct);
            $container->addSelect('employee', 'Tatér', $itemEmployee);
            $container->addHidden('sale_product_id');
        }, 1, 5);

        $productMulti->addCreateButton('Přidat proceduru')
            ->addClass('btn btn-primary');
        $productMulti->addRemoveButton('Odebrat proceduru')
            ->addClass('btn btn-danger');

        //ULOZIT
		$form->addSubmit('send', 'Uložit');

		$form->onValidate[] = [$this, 'formValidate'];
		$form->onSuccess[] = [$this, 'formSuccess'];

		return $form;
	}
	
	public function formValidate(Form $form, array $data): void
    {

        $data['customer'] = $form->getHttpData(Form::DATA_TEXT, 'customer');
        $saleData = new SaleFormData($data);

        $errors = $saleData->validate();
		foreach ($errors as $error) {
			$form->addError($error);
		}
	}
	
	public function formSuccess(Form $form, array $data): void
    {
        $data['customer'] = $form->getHttpData(Form::DATA_TEXT, 'customer');
        $saleData = new SaleFormData($data);
        if (!empty($saleData->customer)) {
            $this->saleRepository->saveSale($saleData);
        }
	}

    private function getProductItems(): array
    {
        $result = [];
        foreach ($this->productRepository->findProducts() as $product) {
            $result[$product->id] = $product->name;
        }
        return $result;
    }

    private function getEmpItems(): array
    {
        $result = [];
        foreach ($this->empRepository->findEmps() as $employee) {
            $result[$employee->id] = $employee->surname . ' ' . $employee->name;
        }
        return $result;
    }

}
