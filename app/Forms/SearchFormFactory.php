<?php
namespace App\Forms;
use Nette\Application\UI\Form;

class SearchFormFactory {
	
	public function create(): Form {
		$form = new Form();
		$form->addSelect('search')
			->setHtmlAttribute('class', 'chosen-select-ajax-search')
			->setHtmlAttribute('data-placeholder', 'Jméno zákaznice nebo tel. číslo');
		$form->addSubmit('send');

		return $form;
	}
}
