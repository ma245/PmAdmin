<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Model\FileRepository;
use Nette\Application\Responses\FileResponse;


final class FilePresenter extends BaseAdminPresenter
{

    private FileRepository $fileRepository;
	
	public function __construct(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
	}

    public function actionDownload(int $id): void
    {
        $file = $this->fileRepository->getFile($id);
        if (!$file) {
            //chyba
        }
        $response = new FileResponse($file->path);
        $this->sendResponse($response);
    }
	
}
