<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Application\UI\Form;

final class SignPresenter extends Nette\Application\UI\Presenter
{

	public function actionOut()
    {
		$this->user->logout();
		$this->redirect('in');
	}
	
	public function actionIn()
    {
	
	}

	protected function createComponentSignInForm(): Form
    {
		$form = new Form;
		$form->addText('login', 'Login')
			->setRequired('Prosím vyplňte své uživatelské jméno/email.');

		$form->addPassword('password', 'Heslo:')
			->setRequired('Prosím vyplňte své heslo.');

		$form->addSubmit('send', 'Přihlásit');

		$form->onSuccess[] = [$this, 'signInFormSucceeded'];
		return $form;
	}
	
	public function signInFormSucceeded(Form $form, \stdClass $values): void
    {
		try {
			$this->getUser()->login($values->login, $values->password);
			$this->redirect('Dashboard:default');
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Nesprávné přihlašovací jméno nebo heslo.');
		}
	}

	
}
