<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;


use App\Forms\VoucherFormFactory;
use App\Model\VoucherRepository;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\DateTime;

/**
 * Prehled a sprava voucheru
 */
final class VoucherPresenter extends BaseAdminPresenter
{

    /** @var VoucherRepository */
    private VoucherRepository $repository;

    /** @var ActiveRow|null */
    private ?ActiveRow $voucher;

    /** @var VoucherFormFactory @inject */
    public VoucherFormFactory $voucherFormFactory;

    public function __construct(VoucherRepository $repository)
    {
        $this->repository = $repository;
    }

    public function renderDefault(): void
    {
        $params = $this->request->getParameters();
        $this->template->vouchers = $this->repository->findVoucherByParams($params);
        $this->template->voucherTypes = $this->repository->findVoucherType();
        //filtr params
        $this->template->options = (array_key_exists('options', $params)) ? $params['options'] : 'all';
        $this->template->types = (array_key_exists('type', $params)) ? $params['type'] : [];
    }

    public function actionNew(): void
    {

    }

    public function actionEdit(int $id): void
    {
        $this->voucher = $this->repository->getVoucher($id);
        if (!$this->voucher) {
            $this->flashMessage('Poukaz nenalezen. Neplatné ID.', 'danger');
            $this->redirect('default');
        }
    }

    protected function createComponentVoucherForm(): Form
    {
        $form = $this->voucherFormFactory->create();
        $form->addButton('back', 'Zpět')
            ->setHtmlAttribute('class', 'btn-back')
            ->setHtmlAttribute('link', $this->link('default'));

        if (isset($this->voucher)) {
            $defaultValues = $this->voucher->toArray();
            if (!empty($this->voucher->date_order)) {
                $defaultValues['date_order'] = $this->voucher->date_order->format('d.m.Y');
            }
            if (!empty($this->voucher->date_validity)) {
                $defaultValues['date_validity'] = $this->voucher->date_validity->format('d.m.Y');
            }
            if (!empty($this->voucher->date_use)) {
                $defaultValues['date_use'] = $this->voucher->date_use->format('d.m.Y');
            }
            if (!empty($this->voucher->date_confirm)) {
                $defaultValues['date_confirm'] = $this->voucher->date_confirm->format('d.m.Y');
            }
            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Editace poukazu proběhla v pořádku', 'success');
            };
        } else {
            $now = new DateTime();
            $validity = new DateTime();
            $validity->modify('+1 year');

            $defaultValues = [
                'date_order' => $now->format('d.m.Y'),
                'date_validity' => $validity->format('d.m.Y')
            ];

            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Uložení poukazu proběhlo v pořádku', 'success');
            };
        }

        $form->setDefaults($defaultValues);

        return $form;
    }
}
