<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;


use App\Model\VoucherOrderRepository;

/**
 * Prehled a sprava objednavek voucheru pres web
 */
final class VoucherOrderPresenter extends BaseAdminPresenter
{
    private VoucherOrderRepository $repository;

    public function __construct(VoucherOrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function renderDefault(): void
    {
        $this->template->vouchers = $this->repository->findVoucherWebOrder();

    }
}
