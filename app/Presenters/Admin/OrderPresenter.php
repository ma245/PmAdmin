<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Forms\OrderFormFactory;
use App\Forms\Data\OrderFormData;
use App\Model\OrderRepository;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;

final class OrderPresenter extends BaseAdminPresenter
{

    /** @var int|null @persistent */
    public ?int $page;

    /** @var OrderRepository */
    private OrderRepository $orderRepository;

    /** @var ActiveRow|null */
	private ?ActiveRow $order;

	/** @var OrderFormFactory @inject */
	public OrderFormFactory $orderFormFactory;
	
	public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
		parent::__construct();
	}
	
	public function renderDefault(int $page = 1): void
    {
        $this->template->states = OrderFormData::getOrderStates();

        $lastPage = 0;
		$this->template->orders = $this->orderRepository->findOrders()->page($page, 20, $lastPage);
        $this->template->page = $page;
        $this->template->lastPage = $lastPage;
	}
	
	public function actionNew()
    {
		
	}
	
	public function actionEdit(int $id)
    {
		$this->order = $this->orderRepository->getOrder($id);
		if (!$this->order) {
			$this->flashMessage('Objednávka nenalezena. Neplatné ID.', 'danger');
			$this->redirect('default');
		}
	}
	
	protected function createComponentOrderForm(): Form
    {
		$form = $this->orderFormFactory->create();
        //ZPET
        $form->addButton('back', 'Zpět')
            ->setHtmlAttribute('class', 'btn-back')
            ->setHtmlAttribute('link', $this->link('default'));

		if (isset($this->order)) {
			//nastavit vychozi hodnoty pro edit
            $data = $this->order->toArray();
            //nastavit souvisejici produkty/procedury
            foreach ($this->order->related('order_product')->fetchAll() as $product) {
                $productData = $product->toArray();
                $data['products'][] = $productData['product_id'];
            }
            if (!empty($this->order->date)) {
                $data['date'] = $this->order->date->format('d.m.Y H:i');
            }
			$form->setDefaults($data);

            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Editace objednávky proběhla v pořádku', 'success');
            };
		} else {
            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Uložení objednávky proběhlo v pořádku', 'success');
            };
        }
		return $form;
	}
	
	public function handleChangeOrderState(int $id, int $state)
    {
		$this->order = $this->orderRepository->getOrder($id);
		if (!$this->order) {
			$this->flashMessage('Objednávka nenalezena. Neplatné ID.', 'danger');
			$this->redirect('default');
		}
        try {
            $res = $this->orderRepository->orderStateTransition($this->order, $state);
        } catch (\Exception $e) {
            $this->flashMessage($e->getMessage(), 'danger');
            $this->redirect('default');
        }

		($res)	? $this->flashMessage('Změna stavu OK.', 'success') 
				: $this->flashMessage('Změnu se nepodařilo provést', 'danger');
		
	}
	
}
