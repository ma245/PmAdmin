<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Forms\VoucherTypeFormFactory;
use App\Model\VoucherTypeRepository;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;

/**
 * Prehled a sprava typů Voucheru
 */
final class VoucherTypePresenter extends BaseAdminPresenter
{

    /** @var VoucherTypeRepository */
    private VoucherTypeRepository $repository;

    /** @var ActiveRow|null */
    private ?ActiveRow $voucherType;

    /** @var VoucherTypeFormFactory @inject */
    public $voucherTypeFactory;


    public function __construct(VoucherTypeRepository $repository)
    {
        $this->repository = $repository;
        $this->voucherType = null;
    }

    public function renderDefault(): void
    {
        $this->template->types = $this->repository->findVoucherType();
    }

    public function actionNew()
    {

    }

    public function actionEdit(int $id)
    {
        $this->voucherType = $this->repository->getVoucherType($id);
        if (!$this->voucherType) {
            $this->flashMessage('Typ poukazu nenalezen. Neplatné ID.', 'danger');
            $this->redirect('default');
        }
    }

    protected function createComponentVoucherTypeForm(): Form
    {
        $form = $this->voucherTypeFactory->create();
        $form->addButton('back', 'Zpět')
            ->setHtmlAttribute('class', 'btn-back')
            ->setHtmlAttribute('link', $this->link('default'));

        if (isset($this->voucherType)) {
            $defaultValues = $this->voucherType->toArray();
            $form->setDefaults($defaultValues);

            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Editace typu poukazu proběhla v pořádku', 'success');
            };
        } else {
            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Uložení nového typu poukazu proběhlo v pořádku', 'success');
            };
        }

        return $form;
    }

}
