<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Forms\SaleFormFactory;
use App\Model\SaleRepository;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;

final class SalePresenter extends BaseAdminPresenter
{
    /** @var int|null @persistent */
    public ?int $page;

    /** @var SaleRepository */
	private SaleRepository $saleRepository;

    /** @var SaleFormFactory @inject */
    public SaleFormFactory $saleFormFactory;

	/** @var ActiveRow|null */
	private ?ActiveRow $sale;
	
	public function __construct(SaleRepository $saleRepository)
    {
		$this->saleRepository = $saleRepository;
        $this->sale = null;
	}
	
	public function renderDefault(int $page = 1): void
    {
        $lastPage = 0;
		$this->template->sales = $this->saleRepository->findSales()->page($page, 20, $lastPage);
        $this->template->page = $page;
        $this->template->lastPage = $lastPage;
	}
	
	public function actionNew()
    {
		
	}
	
	public function actionEdit(int $id)
    {
		$this->sale = $this->saleRepository->getSale($id);
		if (!$this->sale) {
			$this->flashMessage('Prodej nenalezen. Neplatné ID.', 'danger');
			$this->redirect('default');
		}
	}
	
	public function renderEdit()
    {
        $this->template->sale = $this->sale;
        $this->template->sales = $this->saleRepository
            ->findSales()
            ->where(['customer' => $this->sale->customer]);
	}
	
	protected function createComponentSaleForm(): Form
    {
		$form = $this->saleFormFactory->create($this->sale);
        //ZPET
        $form->addButton('back', 'Zpět')
            ->setHtmlAttribute('class', 'btn-back')
            ->setHtmlAttribute('link', $this->link('default'));

		if (isset($this->sale)) {
			//nastavit vychozi hodnoty pro edit
            $products = [];
            foreach ($this->sale->related('sale_product') as $product) {
                $products[] = [
                    'product' => $product->product,
                    'employee' => $product->employee,
                    'sale_product_id' => $product->id
                ];
            }
			$form->setDefaults([
                'id' => $this->sale->id,
                'date' => $this->sale->date->format('d.m.Y H:i'),
                'note' => $this->sale->note,
                'products' => $products
            ]);

            //nastavit FM
            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Editace prodejky proběhla v pořádku', 'success');
            };
		} else {
            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Uložení prodejky proběhlo v pořádku', 'success');
            };
        }
		return $form;
	}
	
}
