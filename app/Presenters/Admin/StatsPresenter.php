<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Model\StatsRepository;

final class StatsPresenter extends BaseAdminPresenter
{
	/*
	 * Statistiky default - Prehled
	 *
	 * Trzby (mesicni)
	 * Trzby (rocni prumer)
	 * Objednavky (cekajici)
	 *
	 * Procedury (mesicni) - jednotlive a srovnani dle rocniho prumeru
	 *
	 * Trzby (rocni prehled)
	 * Procedury mesicni dle tateru
	 * Procedury mesicni dle typu
	 * Objednavky mesicni dle recepcni
	 *
	 * Statistiky earn - Trzby
	 * - tabulka
	 */

    private StatsRepository $repository;

    public function __construct(StatsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function actionDefault(): void
    {

    }

    public function renderDefault(): void
    {
        $this->template->orderPendings = $this->repository->getPendingOrderCount();
        $this->template->earnStats = $this->repository->calcYearEarnMonthly();

    }
}
