<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Forms\PriceListFormFactory;
use App\Model\PriceListRepository;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;

final class PriceListPresenter extends BaseAdminPresenter
{
    /** @var PriceListRepository  */
    private PriceListRepository $priceListRepository;

    /** @var ActiveRow|null */
	private ?ActiveRow $priceList;

    /** @var PriceListFormFactory @inject */
    public PriceListFormFactory $priceListFormFactory;

	public function __construct(PriceListRepository $priceListRepository)
    {
        $this->priceListRepository = $priceListRepository;
        $this->priceList = null;

		parent::__construct();
	}
	
	public function renderDefault()
    {
		$this->template->priceLists = $this->priceListRepository->findActualPriceList();
	}
	
	public function actionNew()
    {
		
	}
	
	public function actionEdit(int $id)
    {
		$this->priceList = $this->priceListRepository->getPriceList($id);
		if (!$this->priceList) {
			$this->flashMessage('Cena nenalezena. Neplatné ID.', 'danger');
			$this->redirect('default');
		}
	}
	
	public function renderEdit()
    {
        $product = $this->priceList->ref('product', 'product');
        $this->template->product = $product;

        $this->template->priceLists = $this->priceListRepository->findPriceListByProduct($product->id);
		
	}
	
	protected function createComponentPriceListForm(): Form
    {
		$form = $this->priceListFormFactory->create();
        //ZPET
        $form->addButton('back', 'Zpět')
            ->setHtmlAttribute('class', 'btn-back')
            ->setHtmlAttribute('link', $this->link('default'));

		if (isset($this->priceList)) {
			//nastavit vychozi hodnoty pro edit
            $data = $this->priceList->toArray();
            if (!empty($this->priceList->valid_from)) {
                $data['valid_from'] = $this->priceList->valid_from->format('d.m.Y');
            }
			$form->setDefaults($data);

            //nastavit FM
            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Editace ceny proběhla v pořádku', 'success');
            };
		} else {
            //nastavit FM
            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Uložení ceny proběhlo v pořádku', 'success');
            };
        }
		return $form;
	}
	

}
