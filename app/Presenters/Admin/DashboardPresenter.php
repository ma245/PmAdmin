<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Forms\Data\OrderFormData;
use App\Forms\OrderFormFactory;
use App\Model\OrderRepository;
use Nette\Application\UI\Form;

final class DashboardPresenter extends BaseAdminPresenter
{

    /** @var OrderRepository  */
    private OrderRepository $orderRepository;

    /** @var OrderFormFactory @inject */
    public OrderFormFactory $orderFormFactory;

	public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
		parent::__construct();
	}
	
	public function renderDefault()
    {
        $this->template->states = OrderFormData::getOrderStates();
		$this->template->dailyPlan = $this->orderRepository->findDailyOrders();
	}

    protected function createComponentOrderForm(): Form
    {
        $form = $this->orderFormFactory->create();

        $form->onSuccess[] = function(Form $form, $data): void {
            $this->flashMessage('Uložení objedávky proběhlo v pořádku', 'success');
        };

        return $form;
    }
	
}
