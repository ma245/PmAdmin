<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Forms\SearchFormFactory;
use App\Model\CustomerRepository;
use Nette\Application\UI\Form;

abstract class BaseAdminPresenter extends \Nette\Application\UI\Presenter {

    /** @var CustomerRepository @inject */
    public CustomerRepository $customerRepository;

	public function __construct()
    {

	}

	protected function startup(): void
    {
		parent::startup();
		
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Sign:in', ['backlink' => $this->storeRequest()]);
		}
	}
	
	
	/** OBSLUHA VYHLEDAVACIHO FORMULARE */ 

	protected function createComponentSearchForm()
    {
		$form = (new SearchFormFactory())->create();
		$form->onSuccess[] = [$this, 'searchFormSuccess'];
		return $form;
	}
	
	public function searchFormSuccess(Form $form, $data)
    {
		$customers_id = $form->getHttpData(Form::DATA_TEXT, 'search');
		if (!$customers_id || !is_numeric($customers_id)) {
			return;
		}
		$this->redirect('Customer:edit', ['id' => $customers_id]);
	}
	
	public function handleSearchTerm()
    {
		$search = $this->getParameter('search');
		$customers = $this->customerRepository->findFilterCustomers($search);
		$data = [];
		foreach ($customers as $customer) {
			$data[] = [
				'id' => $customer->id,
				'text' => $customer->surname . ' ' . $customer->name . ', ' . $customer->phone
			];
		}
		$this->presenter->sendJson($data);
	}

    public function handleGetTerm()
    {
        $search = intval($this->getParameter('search'));
        $customer = $this->customerRepository->getCustomer($search);
        $data = [];
        if ($customer) {
            $data = [
                'id' => $customer->id,
                'text' => $customer->surname . ' ' . $customer->name . ', ' . $customer->phone
            ];
        }
        $this->presenter->sendJson($data);
    }
}
