<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Forms\ProductFormFactory;
use App\Model\ProductRepository;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;

final class ProductPresenter extends BaseAdminPresenter
{

    /** @var ProductRepository  */
    private ProductRepository $productRepository;

    /** @var ActiveRow|null */
    private ?ActiveRow $product;

    /** @var ProductFormFactory @inject */
    public ProductFormFactory $productFormFactory;
	
	public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->product = null;
	}

    public function renderDefault()
    {
        $this->template->products = $this->productRepository->findProducts();
    }

    public function actionNew()
    {

    }

    public function actionEdit(int $id)
    {
        $this->product = $this->productRepository->getProduct($id);
        if (!$this->product) {
            $this->flashMessage('Procedura/produkt nenalezen. Neplatné ID.', 'danger');
            $this->redirect('default');
        }
    }

    public function actionDelete(int $id)
    {
        $this->product = $this->productRepository->getProduct($id);
        if (!$this->product) {
            $this->flashMessage('Procedura/produkt nenalezen. Neplatné ID.', 'danger');
            $this->redirect('default');
        }
        $this->product->delete();
        $this->flashMessage('Procedura/produkt byla odstraněna.', 'success');
        $this->redirect('default');
    }

    protected function createComponentProductForm(): Form
    {
        $form = $this->productFormFactory->create();
        $form->addButton('back', 'Zpět')
            ->setHtmlAttribute('class', 'btn-back')
            ->setHtmlAttribute('link', $this->link('default'));

        if (isset($this->product)) {
            $defaultValues = $this->product->toArray();
            $form->setDefaults($defaultValues);

            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Editace procedury/produktu proběhla v pořádku', 'success');
            };
        } else {
            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Uložení procedury/produktu proběhlo v pořádku', 'success');
            };
        }
        return $form;
    }

	
}
