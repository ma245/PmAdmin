<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Forms\PasswordAdminFormFactory;
use App\Model\EmpRepository;
use Nette\Database\Table\ActiveRow;
use App\Forms\ProfileFormFactory;
use App\Forms\PasswordFormFactory;
use Nette\Application\UI\Form;

final class EmpPresenter extends BaseAdminPresenter
{
    /** @var EmpRepository  */
	private EmpRepository $empRepository;

    /** @var ProfileFormFactory @inject */
	public ProfileFormFactory $profileFormFactory;

    /** @var PasswordAdminFormFactory @inject */
    public PasswordAdminFormFactory $passwordAdminFormFactory;

	/** @var PasswordFormFactory @inject */
	public PasswordFormFactory $passwordFormFactory;

    /** @var ActiveRow|null */
	private ?ActiveRow $emp;
	
	public function __construct(EmpRepository $repository)
    {
		$this->empRepository = $repository;
        $this->emp = null;
	}
	
	public function renderDefault()
    {
		$this->template->emps = $this->empRepository->findEmps();
	}

    public function actionEdit(int $id)
    {
        if (!$this->user->isInRole('ADMIN')) {
            $this->flashMessage('Nemáte "admin" oprávnění.', 'danger');
            $this->redirect('default');
        }
		
        $this->emp = $this->empRepository->getEmp($id);
        if (!$this->emp) {
            $this->flashMessage('Zaměstnanec/uživatel nenalezen. Neplatné ID.', 'danger');
            $this->redirect('default');
        }
    }

	public function actionProfile(int $id)
    {
		if ($id !== $this->user->getId()) {
			$this->flashMessage('Nemáte oprávnění editovat profil někoho jiného', 'danger');
			$this->redirect('default');
		}
		
		$this->emp = $this->empRepository->getEmp($id);
	}

    protected function createComponentPasswordAdminForm(): Form
    {
        $form = $this->passwordAdminFormFactory->create();
        if (isset($this->emp)) {
            //nastavit vychozi hodnoty
            $form->setDefaults(['id' => $this->emp->id]);
        }
        return $form;
    }

	protected function createComponentPasswordForm(): Form
    {
		$form = $this->passwordFormFactory->create();
		if (isset($this->emp)) {
			//nastavit vychozi hodnoty
			$form->setDefaults(['id' => $this->emp->id]);
		}
		return $form;
	}
	
	protected function createComponentProfileForm(): Form
    {
		$form = $this->profileFormFactory->create();
		//ZPET
        $form->addButton('back', 'Zpět')
            ->setHtmlAttribute('class', 'btn-back')
            ->setHtmlAttribute('link', $this->link('default'));

		if (isset($this->emp)) {
			//nastavit vychozi hodnoty pro edit
			$data = $this->emp->toArray();
			unset($data['password']);

            //upravit json
            if (!empty($this->emp->post)) {
                $data['post'] = json_decode($this->emp->post);
            }
            if (!empty($this->emp->role)) {
                $empRole = json_decode($this->emp->role);
                $data['role'] = reset($empRole);
            }

			$form->setDefaults($data);
		}
		$form->onSuccess[] = [$this, 'profileFormSucceeded'];
		
		return $form;
	}
	
	public function profileFormSucceeded(Form $form): void
    {
		if ($form->hasErrors()) {
			$this->flashMessage('Chyba', 'danger');
		} else {
			$this->flashMessage('Záznam byl uložen v pořádku', 'primary');
		}
		$form->cleanErrors();
	}
	
}
