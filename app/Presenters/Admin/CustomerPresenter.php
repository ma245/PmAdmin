<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Model\ProductRepository;
use App\Forms\CustomerFormFactory;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;

final class CustomerPresenter extends BaseAdminPresenter
{

    /** @var int|null @persistent  */
    public ?int $page;

	/** @var CustomerFormFactory @inject */
	public CustomerFormFactory $customerFormFactory;

    /** @var ProductRepository @inject */
    public ProductRepository $productRepository;

    /** @var ActiveRow|null */
	private ?ActiveRow $customer;
	
	public function __construct()
    {

	}
	
	public function renderDefault(int $page = 1): void
    {
        $lastPage = 0;
        $customers = $this->customerRepository->findCustomers()->page($page, 20, $lastPage);

		$this->template->customers = $customers;
        $this->template->page = $page;
        $this->template->lastPage = $lastPage;
	}
	
	public function actionNew()
    {
		
	}
	
	public function actionEdit(int $id)
    {
		$this->customer = $this->customerRepository->getCustomer($id);
		if (!$this->customer) {
			$this->flashMessage('Zákazník nenalezen. Neplatné ID.', 'danger');
			$this->redirect('default');
		}
	}
	
	public function renderEdit()
    {
        $sales = $this->customer->related('sale', 'customer');
		$this->template->sales = $sales;
		$this->template->photos = [];
		$this->template->customer = $this->customer;

		$this->template->products = $this->productRepository->findProducts();

	}
	
	protected function createComponentCustomerForm(): Form
    {
		$form = $this->customerFormFactory->create();
        //ZPET
        $form->addButton('back', 'Zpět')
            ->setHtmlAttribute('class', 'btn-back')
            ->setHtmlAttribute('link', $this->link('default'));

		if (isset($this->customer)) {
			//nastavit vychozi hodnoty pro edit
			$form->setDefaults($this->customer->toArray());

            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Editace zákazníka proběhla v pořádku', 'success');
            };
		} else {
            $form->onSuccess[] = function(Form $form, $data): void {
                $this->flashMessage('Uložení zákazníka proběhlo v pořádku', 'success');
            };
        }

		return $form;
	}
	
}
