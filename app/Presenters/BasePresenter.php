<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Localization\Translator;


abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	/** @persistent */
	public $locale;
	
	/** @var Translator @inject */
	public Translator $translator;
	
	protected function beforeRender(): void {
		parent::beforeRender();
		$this->template->locale = $this->locale;
	}
}
