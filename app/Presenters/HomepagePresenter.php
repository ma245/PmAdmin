<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Forms\VoucherForm;
use Nette\Application\UI\Form;


final class HomepagePresenter extends BasePresenter
{
	/** @var VoucherForm @inject */
	public VoucherForm $voucherFormFactory;
	
	public function actionVoucher()
    {
		$this->template->ogParams = [
			'url'	=> $this->getHttpRequest()->getUrl(),
			'title' => $this->translator->translate('messages.voucherPermanent'),
			'description' => $this->translator->translate('messages.voucherPresent') . ' ' . $this->translator->translate('messages.voucherValid') . ' ' . $this->translator->translate('messages.voucherInterest'),
			'img'	=> "/img/voucher/voucher_winter_{$this->translator->getLocale()}.jpg"
		];
	}
	
	protected function createComponentVoucherForm(): Form
    {
		$form = $this->voucherFormFactory->create();
		$form->onSuccess[] = function (Form $form, $values) {
			$fm = $this->translator->translate('messages.fmVoucher', ['tel' => $values->phone]);
			$this->flashMessage($fm, 'success');
		};
		return $form;
	}
}
