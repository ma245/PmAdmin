<?php

namespace App\Utils\File;

use App\Forms\Data\SaleFormData;
use Nette\Http\FileUpload;
use Nette\Utils\Image;

class FileUploadMover
{
    public static function moveVertrag(SaleFormData $data): ?array
    {
        $vertrag = $data->vertrag;
        if (!$vertrag->hasFile() || !$vertrag->isOk()) {
            return null;
        }
        //presunout z tempu a prejmenovat
        $dest = 'customer/' . $data->customer . '/' . $data->id . '/vertrag';
        $vertrag->move($dest);

        if ($vertrag->isImage()) {
            //vytvorit nahled
            $image = Image::fromFile($dest);
            $image->resize(200, 350);
            $image->save('vertrag_thm.jpg');
        }

        //je obrazek, vytvorit nahled
    }

}