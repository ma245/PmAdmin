<?php
namespace App\Utils;

use Nette\Utils\DateTime;

class YearEarnMonthlyStats
{

    private array $stats = [];

    private $earnYear = 0;

    private $earnYearAverage = 0;

    private $earnActual = 0;

    private $startPeriod;

    public function __construct()
    {
        $year = new DateTime('first day of this month');
        $year->modify('-1 month');
        $year->modify('-1 year');
        $this->startPeriod = clone $year;

        //vytvorit strukturu pro rocni data + aktualni mesic
        $now = new DateTime();
        while ($year < $now) {
            $key = $year->format('Y-m');
            $this->stats[$key]['sum'] = 0;
            $this->stats[$key]['product'] = [];
            $this->stats[$key]['employee'] = [];
            $year->modify('+1 month');
        }
    }

    public function getStartPeriod(): DateTime
    {
        return $this->startPeriod;
    }

    public function getEarnYearAverage(): int
    {
        return $this->earnYearAverage;
    }

    public function getEarnMonthActual(): int
    {
        return $this->earnActual;
    }

    public function getEarnYear(): int
    {
        return $this->earnYear;
    }

    public function addRow(array $data): void
    {
        $date = $data['date'];
        $key = $date->format('Y-m');
        if (!array_key_exists($key, $this->stats)) {
            return;
        }
        $this->stats[$key]['sum'] += $data['amount_czk'];
        $this->stats[$key]['employee'][] = $data['employee'];
        $this->stats[$key]['product'][] = $data['product'];

    }

    public function calc(): void
    {
        $now = new DateTime();
        $actualKey = $now->format('Y-m');
        foreach ($this->stats as $key => $res) {
            if ($actualKey === $key) {
                $this->earnActual += $res['sum'];
            } else {
                $this->earnYear += $res['sum'];
            }
        }
        $this->earnYearAverage = $this->earnYear / 12;
    }
    
    public function getYearEarnMonthlyLineChartData(): array
    {
        $statsValues = [];
        foreach ($this->stats as $key => $res) {
            $statsValues[] = $res['sum'];
        }

        return [
            'labels' => array_keys($this->stats),
            'datasets' => [
                [
                    'label' => 'Tržby',
                    'lineTension'=> 0.3,
                    'backgroundColor'=> "rgba(78, 115, 223, 0.05)",
                    'borderColor'=> "rgba(78, 115, 223, 1)",
                    'pointRadius'=> 3,
                    'pointBackgroundColor'=> "rgba(78, 115, 223, 1)",
                    'pointBorderColor'=> "rgba(78, 115, 223, 1)",
                    'pointHoverRadius'=> 3,
                    'pointHoverBackgroundColor'=> "rgba(78, 115, 223, 1)",
                    'pointHoverBorderColor'=> "rgba(78, 115, 223, 1)",
                    'pointHitRadius'=> 10,
                    'pointBorderWidth'=> 2,
                    'data' => $statsValues
                ]
            ]
        ];
    }

    public function getYearProductPieChartData(): array
    {
        $total = 0; $ld = 0; $lh = 0; $k = 0; $ks = 0; $o = 0;
        foreach ($this->stats as $key => $res) {
            foreach ($res['product'] as $product) {
                $total++;
                if (in_array($product, [1, 6])) {
                    $ld++;
                }
                if (in_array($product, [2, 7])) {
                    $lh++;
                }
                if (in_array($product, [3, 8])) {
                    $k++;
                }
                if (in_array($product, [4, 9])) {
                    $ks++;
                }
                if (in_array($product, [5, 10])) {
                    $o++;
                }
            }
        }
        return [
            'labels' => ['Linky horní', 'Linky dolní', 'Kontura', 'Kontura stín', 'Obočí'],
            'datasets' => [
                [
                    'data'=> [
                        ($lh/$total)*100, ($ld/$total)*100, ($k/$total)*100, ($ks/$total)*100, ($o/$total)*100,
                    ],
                    'backgroundColor'=>     ['#4e73df', '#1cc88a', '#36b9cc', '#72de4e', '#de714e'],
                    'hoverBackgroundColor'=>['#2e59d9', '#17a673', '#2c9faf', '#43ab20', '#ab4220'],
                    'hoverBorderColor'=> "rgba(234, 236, 244, 1)",
                ]
            ]
        ];
    }


}