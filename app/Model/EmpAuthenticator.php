<?php
declare(strict_types=1);

namespace App\Model;

use Nette;
use Nette\Database\Explorer;
use Nette\Security\Passwords;
use Nette\Security\Authenticator;
use Nette\Security\SimpleIdentity;

class EmpAuthenticator implements Authenticator {

	private Explorer $database;
	private Passwords $passwords;

	public function __construct(Explorer $database, Passwords $passwords) {
		$this->database = $database;
		$this->passwords = $passwords;
	}

	public function authenticate(string $username, string $password): SimpleIdentity {

		$row = $this->database->table('employee')
			->where('email', $username)
			->fetch();
		
		if (!$row) {
			throw new Nette\Security\AuthenticationException('Nesprávné přihlašovací údaje');
		}

		if (!$this->passwords->verify($password, $row->password)) {
			throw new Nette\Security\AuthenticationException('Nesprávné přihlašovací údaje');
		}
        $empRole = json_decode($row->role);
        $role = (is_array($empRole) && !empty($empRole)) ? $empRole : ['user'];

		return new SimpleIdentity($row->id, $role, ['username' => $row->email]);
	}
}
