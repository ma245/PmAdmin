<?php
declare(strict_types=1);

namespace App\Model;

use App\Forms\Data\CustomerFormData;
use Nette;
use Nette\Database\Explorer;

class CustomerRepository {
	use Nette\SmartObject;

	private Explorer $database;

    private string $table = 'customer';
	
	public function __construct(Explorer $database)
    {
		$this->database = $database;
	}
	
	public function saveCustomer(CustomerFormData $data)
    {
		if (empty($data->id)) {
			return $this->newCustomer($data);
		} else {
			return $this->updateCustomer($data);
		}
	}
	
	protected function newCustomer(CustomerFormData $data)
    {
        $dbData = $data->__serialize();
		unset($dbData['id']);
		return $this->database->table($this->table)
			->insert($dbData);
	}
	
	protected function updateCustomer(CustomerFormData $data)
    {
        $dbData = $data->__serialize();
		return $this->database->table($this->table)
			->where('id', $data->id)
			->update($dbData);
	}
	
	public function findCustomers(): Nette\Database\Table\Selection
    {
		return $this->database->table($this->table)
            ->order('surname, name');
	}

    public function findFilterCustomers(?string $search)
    {
        $rows = $this->database
            ->table($this->table)
            ->limit(20);
        if (!empty($search)) {
            $rows->whereOr([
                'name LIKE ?'	=> "%$search%",
                'surname LIKE ?'=> "%$search%",
                'phone LIKE ?'	=> "%$search%"
            ]);
        }
        return $rows;
    }
	
	public function getCustomer(int $id)
    {
		return $this->database->table($this->table)->get($id);
	}

    public function deleteAllCustomers()
    {
        return $this->database->table($this->table)->delete();
    }
	
}
