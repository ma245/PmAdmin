<?php

namespace App\Model;

use App\Forms\Data\FileFormData;
use App\Forms\Data\SaleFormData;
use Nette;
use Nette\Database\Explorer;
use Nette\Utils\FileSystem;
use Nette\Database\Table\ActiveRow;
use Tracy\Debugger;

class FileRepository
{
	use Nette\SmartObject;

    private string $table = 'file';

	private Explorer $database;

    /** @var string Napr. 'C:\wamp64\www\marta\app' */
    private string $appDir;
	
	public function __construct(string $appDir, Explorer $database)
    {
		$this->database = $database;
        $this->appDir = FileSystem::joinPaths($appDir, '../www/files/vertrag/');
	}

    public function getFile(int $id): ?ActiveRow
    {
        return $this->database
            ->table($this->table)
            ->get($id);
    }

    public function saveVertrag(SaleFormData $data): ?ActiveRow
    {
        $vertrag = $data->vertrag;
        //ma soubor a soubor je v poradku nahran
        if (!$vertrag || !$vertrag->hasFile() || !$vertrag->isOk()) {
            return null;
        }
        $dest = $this->appDir . $data->customer . '/' . $data->id . '_' . $vertrag->getSanitizedName();
        $vertrag->move($dest);

        return $this->database
            ->table($this->table)
            ->insert([
                'type' => FileFormData::FILE_TYPE_CONTRACT,
                'path' => $dest
            ]);
    }

    public function deleteSaleVertrag(?ActiveRow $sale)
    {
        if (!$sale || empty($sale->vertrag)) {
            return;
        }
        $contract = $sale->ref('vertrag');
        FileSystem::delete($contract->path);
        $contract->delete();
    }
	
}
