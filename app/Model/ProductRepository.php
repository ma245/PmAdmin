<?php
declare(strict_types=1);

namespace App\Model;

use App\Forms\Data\ProductFormData;
use Nette;
use Nette\Database\Explorer;

class ProductRepository {

	use Nette\SmartObject;
	
	private Explorer $database;

    private string $table = 'product';
	
	public function __construct(Explorer $database)
    {
		$this->database = $database;
	}
	
	public function saveProduct(ProductFormData $data)
    {
		if (empty($data->id)) {
			$this->newProduct($data);
		} else {
			$this->updateProduct($data);
		}
	}
	
	protected function newProduct(ProductFormData $data)
    {
        $dbData = $data->serialize();
		unset($dbData['id']);

		return $this->database
            ->table($this->table)
			->insert($dbData);
	}
	
	protected function updateProduct(ProductFormData $data)
    {
        $dbData = $data->serialize();

		return $this->database
            ->table($this->table)
			->where('id', $data->id)
			->update($dbData);
	}
	
	public function findProducts()
    {
		return $this->database
            ->table($this->table)
            ->order('id');
	}
	
	public function getProduct(int $id)
    {
		return $this->database
            ->table($this->table)
            ->get($id);
	}
	
}
