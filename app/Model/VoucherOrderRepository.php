<?php

namespace App\Model;

use App\Forms\Data\VoucherOrderFormData;
use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Explorer;
use Tracy\Debugger;

class VoucherOrderRepository {
	use Nette\SmartObject;

	private Explorer $database;

    private string $table = 'voucher_order';

	
	public function __construct(Explorer $database)
    {
		$this->database = $database;
	}

    public function findVoucherWebOrder()
    {
        return $this->database
            ->table($this->table)
            ->order('date_create DESC');
    }

    public function saveVoucherWebOrder(VoucherOrderFormData $data)
    {
        if (empty($data->id)) {
            $this->newWebOrder($data);
        } else {
            $this->updateWebOrder($data);
        }
    }

    private function newWebOrder(VoucherOrderFormData $data)
    {
        $dbData = $data->serialize();
        unset($dbData['id']);

        return $this->database
            ->table($this->table)
            ->insert($dbData);
    }

    private function updateWebOrder(VoucherOrderFormData $data)
    {
        $dbData = $data->serialize();

        return $this->database
            ->table($this->table)
            ->where('id', $data->id)
            ->update($dbData);
    }
	
}
