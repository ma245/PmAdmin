<?php

namespace App\Model;

use App\Utils\YearEarnMonthlyStats;
use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Explorer;
use Tracy\Debugger;

class StatsRepository {
	use Nette\SmartObject;

	private Explorer $database;

	
	public function __construct(Explorer $database)
    {
		$this->database = $database;
	}

    public function getPendingOrderCount(): int
    {
        return $this->database
            ->table('order')
            ->where([
                'date >' => new Nette\Utils\DateTime(),
                'state' => [1, 2]
            ])
            ->count();
    }

    public function calcYearEarnMonthly(): YearEarnMonthlyStats
    {
        $result = new YearEarnMonthlyStats();

        $sql = "SELECT date, sale_product.product, employee, amount_czk 
                FROM sale
                LEFT JOIN sale_product ON sale_product.sale = sale.id
                LEFT JOIN price_list ON price_list.product = sale_product.product
                WHERE date > ?";
        //zjistit vsechny procedury za posledni rok + prislusnou cenu
        $rows = $this->database
            ->query($sql, $result->getStartPeriod())
            ->fetchAll();

        foreach ($rows as $row) {
            $result->addRow((array) $row);
        }
        $result->calc();

        return $result;
    }
	
}
