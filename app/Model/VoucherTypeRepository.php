<?php

namespace App\Model;

use App\Forms\Data\VoucherTypeFormData;
use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Explorer;
use Tracy\Debugger;

class VoucherTypeRepository {
	use Nette\SmartObject;

	private Explorer $database;

    private string $table = 'voucher_type';

	
	public function __construct(Explorer $database)
    {
		$this->database = $database;
	}

    public function save(VoucherTypeFormData $data)
    {
        if (empty($data->id)) {
            $this->newVoucherType($data);
        } else {
            $this->updateVoucherType($data);
        }
    }

    private function newVoucherType(VoucherTypeFormData $data)
    {
        $dbData = $data->serialize();
        unset($dbData['id']);

        return $this->database
            ->table($this->table)
            ->insert($dbData);
    }

    private function updateVoucherType(VoucherTypeFormData $data)
    {
        $dbData = $data->serialize();

        return $this->database
            ->table($this->table)
            ->where('id', $data->id)
            ->update($dbData);
    }

    public function findVoucherType()
    {
        return $this->database
            ->table($this->table)
            ->order('name ASC, id DESC');
    }

    public function getVoucherType(int $id)
    {
        return $this->database
            ->table($this->table)
            ->get($id);
    }

	
}
