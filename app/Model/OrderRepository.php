<?php

namespace App\Model;

use App\Forms\Data\OrderFormData;
use Nette;
use Nette\Utils\DateTime;
use Nette\Database\Row;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Explorer;

class OrderRepository {
	use Nette\SmartObject;

    private string $table = 'order';

	private Explorer $database;
	
	public function __construct(Explorer $database) {
		$this->database = $database;
	}
	
	public function findDailyOrders(?DateTime $date = null) {
		if (!$date) {
			$date = new DateTime();
		}
		$start = $date->format('Y-m-d 00:00:00');
		$end = $date->format('Y-m-d 23:59:59');

        return $this->database
            ->table($this->table)
            ->where('date BETWEEN ? AND ? ', $start, $end)
            ->order('date');
	}
	
	public function findOrders(): Nette\Database\Table\Selection {
        return $this->database
            ->table($this->table)
            ->order('date DESC');
	}
	
	public function findOrdersDetail(array $orderIds) {
		//k nim nacist detail objednavky
		$sql = "SELECT oi.order, p.name AS product_name FROM order_items oi "
			. "LEFT JOIN products p ON p.products_id = oi.product "
			. "LEFT JOIN employees e ON e.employees_id = oi.employee "
			. "WHERE oi.order IN (?) ";
		$rows = $this->database->query($sql, $orderIds)->fetchAll();
		return $rows;
	}
	
	public function getOrder(int $id): ?ActiveRow {
		return $this->database->table($this->table)->get($id);
	}

    public function orderStateTransition(ActiveRow $order, int $state) {
        $avalState = OrderFormData::getOrderStatesByState($order->state);
        if (!array_key_exists($state, $avalState)) {
            throw new \Exception('Neplatný stav objednávky');
        }
        $dbData = [
            'state' => $state
        ];
        $stateDate = new DateTime();
        if ($state === 1) {
            $dbData['date_new'] = $stateDate;
        } elseif ($state === 2) {
            $dbData['date_confirm'] = $stateDate;
        } elseif ($state === 3) {
            $dbData['date_realization'] = $stateDate;
        } elseif ($state === 4) {
            $dbData['date_cancel'] = $stateDate;
        }
        return $order->update($dbData);
    }

	public function saveOrder(OrderFormData $data) {
		if (empty($data->id)) {
			$this->newOrder($data);
		} else {
			$this->updateOrder($data);
		}
	}
	
	protected function newOrder(OrderFormData $data) {
		$orderData = $data->serialize();
        unset($orderData['id']);

		/** @var Nette\Database\Table\ActiveRow $result */
		$result = $this->database->table($this->table)
			->insert($orderData);
		
		if ($result && !empty($data->products)) {
			foreach ($data->products as $product) {
				$orderProductData = [
					'order_id' => $result->id,
					'product_id' => $product
				];
				$this->database->table('order_product')->insert($orderProductData);
			}
		}
		return $result;
	}
	
	protected function updateOrder(OrderFormData $data) {
        $dbData = $data->serialize();
		$result = $this->database->table($this->table)
			->where('id', $data->id)
			->update($dbData);

        if ($result) {
            //odstranit vsechny vazby z order_product
            $this->database
                ->table('order_product')
                ->where('order_id', $data->id)
                ->delete();
            //nastavit je znovu
            foreach ($data->products as $product) {
                $orderProductData = [
                    'order_id' => $data->id,
                    'product_id' => $product
                ];
                $this->database->table('order_product')->insert($orderProductData);
            }
        }
	}
	
}
