<?php

namespace App\Model;

use App\Forms\Data\VoucherFormData;
use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Explorer;
use Tracy\Debugger;

class VoucherRepository {
	use Nette\SmartObject;

	private Explorer $database;

    private string $table = 'voucher';

	
	public function __construct(Explorer $database)
    {
		$this->database = $database;
	}

    public function save(VoucherFormData $data)
    {
        if (empty($data->id)) {
            $this->newVoucher($data);
        } else {
            $this->updateVoucher($data);
        }
    }

    private function newVoucher(VoucherFormData $data)
    {
        $dbData = $data->serialize();
        unset($dbData['id']);

        return $this->database
            ->table($this->table)
            ->insert($dbData);
    }

    private function updateVoucher(VoucherFormData $data)
    {
        $dbData = $data->serialize();
        unset($dbData['id']);

        return $this->database
            ->table($this->table)
            ->where('id', $data->id)
            ->update($dbData);
    }

    public function findVoucher()
    {
        return $this->database
            ->table($this->table)
            ->order('number');
    }

    public function findVoucherByParams(array $params)
    {
        $result = $this->database
            ->table($this->table)
            ->order('number');

        //vse/nevycerpane/vycerpane
        if (array_key_exists('options', $params)) {
            if ($params['options'] == 'unused') {
                $result->where('date_use IS NULL');
            } elseif ($params['options'] == 'used') {
                $result->where('date_use IS NOT NULL');
            }
        }
        //typ
        if (array_key_exists('type', $params)) {
            $result->where('type', $params['type']);
        }

        return $result;
    }

    public function getVoucher(int $id)
    {
        return $this->database
            ->table($this->table)
            ->get($id);
    }

    public function findVoucherType()
    {
        return $this->database
            ->table('voucher_type')
            ->order('name');
    }

    public function deleteAllVouchers()
    {
        return $this->database
            ->table($this->table)
            ->delete();
    }
}
