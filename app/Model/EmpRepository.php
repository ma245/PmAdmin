<?php

namespace App\Model;

use App\Forms\Data\EmployeeFormData;
use App\Forms\Data\EmployeePasswordFormData;
use Nette;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Explorer;
use Nette\Security\Passwords;
use Tracy\Debugger;

class EmpRepository
{
	use Nette\SmartObject;

	private Explorer $database;
    private Passwords $passwords;
    private string $table = 'employee';
	
	public function __construct(Explorer $database, Passwords $passwords)
    {
		$this->database = $database;
        $this->passwords = $passwords;
	}

    public function saveEmployee(EmployeeFormData $data)
    {
        if (empty($data->id)) {
            $this->newEmp($data);
        } else {
            $this->editEmp($data);
        }
    }
	
	public function findEmps()
    {
		return $this->database
            ->table($this->table)
            ->order('name');
	}
	
	public function getEmp(int $id): ActiveRow
    {
		return $this->database
            ->table($this->table)
            ->get($id);
	}
	
	public function newEmp(EmployeeFormData $data)
    {
		$dbData = $data->serialize();
        unset($dbData['id']);
        //vychozi heslo
        $newDefaultPwd = $this->passwords->hash(EmployeeFormData::EMP_DEF_PWD);
        $dbData['password'] = $newDefaultPwd;

		return $this->database
            ->table($this->table)
			->insert($dbData);
	}
	
	public function editEmp(EmployeeFormData $data): bool
    {
		$dbData = $data->serialize();
        if (array_key_exists('password', $dbData) && !empty($dbData['password'])) {
            $dbData['password'] = $this->passwords->hash($dbData['password']);
        }
        return $this->database->table($this->table)
            ->where('id', $data->id)
            ->update($dbData);
	}

    public function editEmpPwd(EmployeePasswordFormData $data)
    {
        $dbData = $data->serialize();
        if (array_key_exists('password', $dbData) && !empty($dbData['password'])) {
            $dbData['password'] = $this->passwords->hash($dbData['password']);
        }
        return $this->database->table($this->table)
            ->where('id', $data->id)
            ->update($dbData);
    }
	
}
