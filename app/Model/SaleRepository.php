<?php
declare(strict_types=1);

namespace App\Model;

use App\Forms\Data\SaleFormData;
use Nette;
use Nette\Database\Explorer;

class SaleRepository {

	use Nette\SmartObject;

	private Explorer $database;

    private SaleProductRepository $saleProductRepository;

    private FileRepository $fileRepository;

    private string $table = 'sale';
	
	public function __construct(
        Explorer $database,
        SaleProductRepository $saleProductRepository,
        FileRepository $fileRepository)
    {
		$this->database = $database;
        $this->saleProductRepository = $saleProductRepository;
        $this->fileRepository = $fileRepository;
	}
	
	public function saveSale(SaleFormData $data)
    {
		if (empty($data->id)) {
			$this->newSale($data);
		} else {
			$this->updateSale($data);
		}
	}
	
	protected function newSale(SaleFormData $data)
    {
        $dbData = $data->serialize();
		unset($dbData['id']);
		$result = $this->database->table($this->table)
			->insert($dbData);

        if (!$result) {
            return $result;
        }
        $data->id = $result->id;
        //ulozit vertrag
        $dbFile = $this->fileRepository->saveVertrag($data);
        if ($dbFile) {
            $result->update(['vertrag' => $dbFile->id]);
        }

        //ulozit produkty/procedury
        foreach ($data->serializeProducts() as $product) {
            $this->saleProductRepository->saveSaleProduct($product);
        }
	}
	
	protected function updateSale(SaleFormData $data)
    {
        //puvodni zaznam
        $sale = $this->getSale($data->id);
        //ulozit vertrag
        $dbFile = $this->fileRepository->saveVertrag($data);

        $dbData = $data->serialize();
        if ($dbFile) {
            //odstranit puvodni
            $this->fileRepository->deleteSaleVertrag($sale);
            //nastavit novy
            $dbData['vertrag'] = $dbFile->id;
        }
        try {
            $this->database->table($this->table)
                ->where('id', $data->id)
                ->update($dbData);
        } catch (\Exception $e) {
            return false;
        }

        //odstranit jiz neexistuji vazby
        $exitsRels = [];
        foreach ($data->serializeProducts() as $product) {
            if ($product->id) {
                $exitsRels[] = $product->id;
            }
        }
        $this->saleProductRepository->removeUnusedSaleProduct($exitsRels);

        //pridat/editovat existuji vazby
        foreach ($data->serializeProducts() as $product) {
            $this->saleProductRepository->saveSaleProduct($product);
        }
	}
	
	public function findSales(): Nette\Database\Table\Selection
    {
		return $this->database->table($this->table)
            ->order('date DESC');
	}
	
	public function getSale(int $id)
    {
		return $this->database
            ->table($this->table)
            ->get($id);
	}

    public function deleteAllSales()
    {
        return $this->database->table($this->table)->delete();
    }
	
}
