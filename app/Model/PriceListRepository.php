<?php

namespace App\Model;

use App\Forms\Data\OrderFormData;
use App\Forms\Data\PriceListFormData;
use Nette;
use Nette\Utils\DateTime;
use Nette\Database\Row;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Explorer;
use Tracy\Debugger;

class PriceListRepository {
	use Nette\SmartObject;

    private string $table = 'price_list';

	private Explorer $database;
	
	public function __construct(Explorer $database)
    {
		$this->database = $database;
	}

    public function findPriceList()
    {
        return $this->database
            ->table($this->table)
            ->order('valid_from DESC');
    }

    public function findActualPriceList()
    {
        $products = $this->database
            ->query('SELECT DISTINCT product FROM price_list')
            ->fetchPairs(null, 'product');

        $result = [];
        $now = new DateTime();
        foreach ($products as $product) {
            $result[] = $this->database
                    ->table($this->table)
                    ->where([
                        'product' => $product,
                        'valid_from <=' => $now
                    ])
                    ->order('valid_from DESC')
                    ->limit(1)
                    ->fetch();
        }
        return $result;
    }

    public function findPriceListByProduct(int $product)
    {
        return $this->database
            ->table($this->table)
            ->where([
                'product' => $product
            ])
            ->order('valid_from DESC');
    }

    public function getPriceList(int $id)
    {
        return $this->database
            ->table($this->table)
            ->get($id);
    }

	public function savePriceList(PriceListFormData $data)
    {
		if (empty($data->id)) {
			$this->newPriceList($data);
		} else {
			$this->updatePriceList($data);
		}
	}
	
	protected function newPriceList(PriceListFormData $data)
    {
		$priceData = $data->serialize();
        unset($priceData['id']);

		/** @var Nette\Database\Table\ActiveRow $result */
		return $this->database->table($this->table)
			->insert($priceData);
	}
	
	protected function updatePriceList(PriceListFormData $data)
    {
        $dbData = $data->serialize();
		return $this->database->table($this->table)
			->where('id', $data->id)
			->update($dbData);
	}
	
}
