<?php

namespace App\Model;

use App\Forms\Data\OrderFormData;
use App\Forms\Data\PriceListFormData;
use App\Forms\Data\SaleProductFormData;
use Nette;
use Nette\Utils\DateTime;
use Nette\Database\Row;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Explorer;
use Tracy\Debugger;

class SaleProductRepository {
	use Nette\SmartObject;

    private string $table = 'sale_product';

	private Explorer $database;
	
	public function __construct(Explorer $database)
    {
		$this->database = $database;
	}

    public function saveSaleProduct(SaleProductFormData $data)
    {
        if (empty($data->id)) {
            $this->newSaleProduct($data);
        } else {
            $this->updateSaleProduct($data);
        }
    }

    public function removeUnusedSaleProduct(array $usedIds)
    {
        return $this->database
            ->table($this->table)
            ->where([
                'id NOT' => $usedIds
            ])
            ->delete();
    }

    private function newSaleProduct(SaleProductFormData $product)
    {
        $dbData = $product->serialize();
        unset($dbData['id']);

        return $this->database
            ->table($this->table)
            ->insert($dbData);
    }

    private function updateSaleProduct(SaleProductFormData $product)
    {
        $dbData = $product->serialize();

        return $this->database
            ->table('sale_product')
            ->where([
                'id' => $dbData['id']
            ])
            ->update($dbData);
    }
	
}
