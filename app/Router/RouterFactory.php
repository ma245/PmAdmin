<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;
		
		$router->withModule('Admin')
				->addRoute('admin/<presenter>/<action>', 'Admin:Sign:in');
		
		$router->addRoute('[<locale=cs cs|de>/][<presenter=Homepage>/]contact', 'Homepage:contact');
		
		$router->addRoute('[<locale=cs cs|de>/][<presenter=Homepage>/]about-oboci', 'Homepage:aboutOboci');
		$router->addRoute('[<locale=cs cs|de>/][<presenter=Homepage>/]about-linky', 'Homepage:aboutLinky');
		$router->addRoute('[<locale=cs cs|de>/][<presenter=Homepage>/]about-rty', 'Homepage:aboutRty');
		$router->addRoute('[<locale=cs cs|de>/][<presenter=Homepage>/]voucher', 'Homepage:voucher');
		
		$router->addRoute('[<locale=cs cs|de>/]<presenter=Homepage>/<action>[/<id>]', 'Homepage:default');
		
		
		
		return $router;
	}
}
